.. _Whaq8VL:

#######
Whaq8VL
#######

.. image:: ../../img/gilly/Whaq8VL.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/Whaq8VL.png>`_

Explination
***********

Read the image text before your read this.

We can see that this is soon likely after Zhe and Eta got back in contact. This likely happened right after :ref:`EAeGxX0`. In
this log they realize that :ref:`Wynn` has been deleted as a result of :ref:`Rigel` and :ref:`Vita`'s attempt to bring Wynn to Stratz's computer in order to
corrupt them. In this log we are introduced the "lotus" it is not mentioned anywhere else in the ARG. We also see that Rigel literally tries to destroy Eta's
computer, we have no idea why. Perhaps in an attempt to stop our :ref:`plan`.

Transcript
**********

::

 eta.tonos: He was super pissed that you left
 eta.tonos: I guess pissed isn't the right word, more like disappointed
 eta.tonos: Anyway we pretty quickly fell out of contact
 eta.tonos: Yeah, you broke up GLY.PH good
 zhe.breve: But nothing was deleted, right?
 eta.tonos: Well at first James and I didn't want to get rid of anything
 eta.tonos: It just would have been a waste really
 eta.tonos: I don't know where the old files for all those projects like rigel or lotus went but they're still there somewhere on my pc I think?
 eta.tonos: Actually some stuff might only be on this computer an i'll be fucked if i know what happended to that
 eta.tonos: I bet those were deleted
 zhe.breve: So do you have *any* idea where he moved to?
 eta.tonos: Somewhere east cost
 eta.tonos: Yeah sorry that's all I've got

 zhe.breve: Sven, all of the files for Wynn on my machine are gone. Did you do something with them?
 eta.tonos: No
 eta.tonos: Did you remove them accidentally or what
 zhe.breve: I'm looking at your computer right now, and your copies are all gone too.
 eta.tonos: The fuck
 eta.tonos: I didn't do anything
 zhe.breve: It's literally just the files for Wynn - Vita and Ayin haven't been touched.
 eta.tonos: Deletion routine malfunction?
 zhe.breve: I guess?
 zhe.breve: That's an incredibly specific malfunction. If some safety check was overridden, I would expect it to, you know, actually delete the files it's supposed to.
 zhe.breve: Not only its own folder.
 eta.tonos: You still have the off-site backups, right
 zhe.breve: Of course.
 zhe.breve: Plugged them into a clean computer, they look fine.
 zhe.breve: This shouldn't matter in the grand scheme of things, but I'm a little uneasy now.
 eta.tonos: Don't play with malware unless you're prepared to lose everything
 eta.tonos: lol

 zhe.breve: Okay Sven, second weird problem of the day.
 zhe.breve: I can't seem to run Vita anymore. It force-closes as soon as it's opened.
 eta.tonos: Post the log?
 zhe.breve: Entirely empty.
 eta.tonos: Ouch, something's really wrong then
 eta.tonos: Recompile it
 zhe.breve: I did. The code all looks fine to me.
 eta.tonos: Neither of us changed it, right
 zhe.breve: Nope
 eta.tonos: Good lord
 eta.tonos: I'm not sure I care enough to fix it, do you
 zhe.breve: No.
 zhe.breve: If we compile it off the backups it might work, but why go through the hassle, really.
 eta.tonos: I don't remember our old projects acting up this much.
 zhe.breve: Tell me about it.

 zhe.breve: I'm off for the day.
 zhe.breve: If anything else strange happens, text me ASAP.
 eta.tonos: sure
 eta.tonos: have fun

 eta.tonos: You wanted me to text about weird things so here's a weird thing
 eta.tonos: 

.. image:: ../../img/gilly/fan.png

::

 eta.tonos: Computer on, CPU fan not running
 eta.tonos: This is my machine
 zhe.breve: The fan broke?
 zhe.breve: That's awfully strange. Are CPU fans supposed to just *break* like that?
 zhe.breve: You better turn everything off, like right now.
 eta.tonos: Don't worry it's off
 eta.tonos: I got the picture and slammed the power strip
 eta.tonos: welp, got to get that replaced as soon as possible or else I could have a disastrous case of exploding CPU
 eta.tonos: How many things am I going to have to fix with this damn machine seriously.
 zhe.breve: You shouldn't have stopped for a picture IMO
 zhe.breve: Literally playing with fire there.
 zhe.breve: That CPU could be permanently damaged now, depending on how long you went without noticing it.
 eta.tonos: I mean, it's supposed to shut off after a certain temperature right
 zhe.breve: *supposed* to
 eta.tonos: I was just working, heard a little click, looked over and saw the fan spool down
 eta.tonos: Thank fuck for transparent computer cases
 eta.tonos: Let's see what else can go wrong today, we're really loving these tech problems