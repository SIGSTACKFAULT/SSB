.. _eTspnH2:

#######
eTspnH2
#######

.. image:: ../../img/mars/eTspnH2.png
   :width: 45%
   
`Original Image <https://imgur.com/eTspnH2>`_

The puzzle here essentially tells us to order all the greek letter marked pages hidden XP backgrounds somehow.
Due to the ARG ending there is no one willing to document the puzzle, so I'll simply link you to the solution

-Phobos

:ref:`S5D1H5OWF0Q`

Background
**********

The edges of this puzzle are the edges of the Windows XP background "Crystal"

.. image:: ../../img/mars/Crystal.jpg
   :width: 45%
   

.. admonition:: TODO
   :class: danger
   
   Everything!