.. _rQwoY8W:

#######
rQwoY8W
#######

.. image:: ../../img/gilly/rQwoY8W.png
   :width: 45%
   
`Original Image <https://imgur.com/rQwoY8W>`_

The first image.

Solution
********

|The sign is the overlay of W and h.
|With the other two images :ref:`TCHCqDR` and :ref:`iZurCs0` this leads to :ref:`Whaq8VL`.

