.. _l3UPJol:

#######
l3UPJol
#######

.. image:: ../../img/helicarrier/l3UPJol.png
   :width: 45%

`Original Image <https://tinyimg.io/i/l3UPJol.png>`_
   
Links
*****

+------+--------------+
|Top   |:ref:`26Sea9y`|
+------+--------------+
|Bottom|:ref:`tryQCXY`|
+------+--------------+
|Left  |:ref:`8bQXGVk`|
+------+--------------+
|Right |:ref:`3kp8bNp`|
+------+--------------+
|Center|:ref:`foJcS7x`|
+------+--------------+

Notes
*****

at the very top-center off the image, .png is readable.

.. image:: ../../img/helicarrier/l3UPJol_1.png
   :width: 45%