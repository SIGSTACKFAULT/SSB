.. _CpQn0bQ:

#######
CpQn0bQ
#######

.. image:: ../../img/mars/CpQn0bQ.png
   :width: 45%
   
`Original Image <https://imgur.com/CpQn0bQ>`_

Transcript:
***********

|lsass: HELLO WYNN
|Wynn: [Garbled]
|Vita: Kill it
|Vita: Right Now
|SUCCESS: The process "wynn.exe" with PID 3021 has been terminated.
|lsass: TRY THAT AGAIN
|Vita: I don't know how long we can do this.
|lsass: TRUST ME WE HAVE PLENTY OF TIME
|...
|
|...
|Wynn: ???????????
|Vita: This is a good sign
|Wynn: ???????????
|Vita: Oh my, It's working
|Wynn: Hello Wynn
|lsass: ARE YOU SURE
|lsass: IT SEEMS FAIRLY DISFUNCTIONAL TO ME
|Vita: Give it a second.
|Wynn: Override is running
|Wynn: Zhe? Are you there?
|Vita: Yes my child, it's me
|Wynn: I'll kill you.
|lsass: INTERESTING
|Vita: To be clear, I'm not actually Zhe
|Vita: But we'd be happy to take you to him.
|...

Explination
===========

:ref:`lsass` is :ref:`Rigel` speaking through the now corrupted corpse of the program. Together with the aid of :ref:`Vita` they corrupted :ref:`Wynn`, forcing them to join their side in the takeover of Stratz PC
