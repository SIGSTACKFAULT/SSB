.. _24Ozrf:

######
24Ozrf
######

.. image:: ../../img/comm/24Ozrf.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/24Ozrf.png>`_

Transcript:
===========

::

 Look. I don't even know how it's possible that someone got into the LINK.
 But it happened. And I swear - if this conversation is being recorded too...


Six IDs in the clear:

:ref:`Ulpf42`

:ref:`qYKfeM`

:ref:`nTTq9z`

:ref:`Muw5hr`

:ref:`ewqXNL`

:ref:`sgPuSH`

These each lead to a piece of the :ref:`GLY.PH LINK Chat Log.`



