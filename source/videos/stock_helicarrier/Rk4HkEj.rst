.. _Rk4HkEj:

#######
Rk4HkEj
#######

.. image:: ../../img/helicarrier/Rk4HkEj.png
   :width: 45%

`Original Image <http://tinyimg.io/i/Rk4HkEj.png>`_

Transcript
**********

|To save us both the trouble, I'll be direct. We are in danger.
|Our Administrator is in danger. Depending on the severity of 
|the unknown varibles, there's a small possibility that the 
|entirety of the human race is in danger - though I shouldn't 
|get ahead of myself. 

:ref:`Continued <Ca5VAj5>`

Stratish
********

Rigel *"At least they are correct about something, danger is all around"*