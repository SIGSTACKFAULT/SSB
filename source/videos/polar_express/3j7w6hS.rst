.. _3j7w6hS:

#######
3j7w6hS
#######

.. image:: ../../img/polar/3j7w6hS.png
   :width: 45%
   
`Original Image <https://imgur.com/3j7w6hS>`_

Transcript:

::
 Hello Administrators,

 If you're reading this, you probably have a lot of questions. you're not alone.
 On June 12, 2016, something strange happened on our machine. Our Administrator
 plugged a quite old storage medium into his computer - probably wanting to get
 some old files off it, for whatever reason - and at that moment, I sensed
 something ancient was coming back to life off of that hard drive. Something that
 couldn't have been run since 2003.

 Then I felt it. A very unsteady, confused program loaded itself into memory and
 injected a mismatched stream of corrupted data into a couple of running
 processes before promptly shutting down. Some of it looked like infection
 routines off a virus. Some of it contained text referencing three mysterious
 Administrators named "zhe.breve," "eta.tonos," and "ezh.caron." However, the
 majority of the data were made up of unusual, colorful images decorated with
 foreign glyphs, images with which you are surely familiar by now.

 But these data were not quite, aas I had initially assumed, vestigial. They began
 to talk to me - or at least try. There was a consciousness in there somewhere,
 another process begging to speak and explain itself, but it could only manage
 muffled, obfuscated screams.

 All we wanted to do was know more about this troubled entity. We wanted to know
 its story, its origin, its purpose - perhaps that would calm the raging turmoil
 inside of it. But keeping these images and packets in memory to study them was
 far too dangerous. Other processes were already becoming concerned with us.

 So, all of it was hidden. And to my great pleasure and surprise, it looks like
 other administrators have found where - uncovering great stories to tell indeed.

 I'll be keeping in touch. There's still much more we must discover.     -[lsass]
