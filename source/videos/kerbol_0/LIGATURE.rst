.. _LIGATURE:

########
LIGATURE
########

File Tree
*********

| ───ligature-0.3b.3
|    │   DISCLAIMER.TXT
|    │   LICENSE.TXT
|    │
|    └───LIGATURE
|        │   ligature.exe
|        │   nd
|        │
|        ├───modules
|        │   │   trail-1.121.ltr
|        │   │
|        │   ├───addons
|        │   │   ├───mod-dst
|        │   │   │       rules.png
|        │   │   │
|        │   │   ├───mod-ntr
|        │   │   │   │   ntr.png
|        │   │   │   │   sol.txt
|        │   │   │   │
|        │   │   │   ├───ν
|        │   │   │   │       1.png
|        │   │   │   │       2.png
|        │   │   │   │       3.png
|        │   │   │   │       4.png
|        │   │   │   │       5.png
|        │   │   │   │       6.png
|        │   │   │   │       7.png
|        │   │   │   │
|        │   │   │   └───ψ
|        │   │   │           1.png
|        │   │   │           2.png
|        │   │   │           3.png
|        │   │   │           4.png
|        │   │   │           5.png
|        │   │   │           6.png
|        │   │   │           7.png
|        │   │   │           trail.5.057.ltr
|        │   │   │
|        │   │   └───mod-pst
|        │   │           order.png
|        │   │           trail-4.081.ltr
|        │   │
|        │   └───common
|        │       │   trail-9.000.ltr
|        │       │
|        │       ├───mod-lct
|        │       │       lct_nd-f2l.log
|        │       │       manual.txt
|        │       │       nd
|        │       │       trail-7.051.ltr
|        │       │       ρ.txt
|        │       │
|        │       ├───mod-mtt
|        │       │       manual.txt
|        │       │       sol.txt
|        │       │       trail-8.000.ltr
|        │       │
|        │       └───mod-mtx
|        │               manual.txt
|        │               trail-6.108.ltr
|        │
|        ├───readme
|        │       Readme.txt
|        │       trail-3.122.ltr
|        │
|        └───resources
|            ├───bitmap
|            │       figure_4.png
|            │       pack.png
|            │       Red moon desert.jpg
|            │       ε.png
|            │       ξ+θ.png
|            │
|            ├───string
|            │       LIGATURE.LCF
|            │       nd
|            │       trail-2.075.ltr
|            │       χ.txt
|            │
|            └───wav
           
           