.. _sgPuSH:

######
sgPuSH
######

.. image:: ../../img/comm/sgPuSH.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/sgPuSH.png>`_

Solve
*****

| from the inner polygon to the outer, the number of sides in base 16: 67 6d 49 6C 77 32
| In ascii:

gmIlw2

.. _gmIlw2:

gmIlw2
******

.. image:: ../../img/comm/gmIlw2.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/gmIlw2.png>`_

Part of :ref:`GLY.PH LINK Chat Log.`

Doodles
*******

A hexagon and six-pointed star.