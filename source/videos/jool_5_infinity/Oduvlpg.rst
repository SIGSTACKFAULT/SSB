.. _Oduvlpg:

#######
Oduvlpg
#######

.. image:: ../../img/jool_5_infinity/Oduvlpg.png
   :width: 45%

`Original Image <https://imgur.com/Oduvlpg>`_

SIY (Solve it yourself)
***********************

Not really solve-able. Just states the graph's function at :ref:`GetPnVy`
