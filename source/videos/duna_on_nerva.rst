#############
Duna on Nerva
#############

.. video :: PPlJVMpoyMA
   :frame: 8:38
   
8:38
****

.. image:: ../img/frames/duna_on_nerva.png
   :width: 45%

Solution
********

The right side mecjeb panel turns into binary, translating it and then putting it into an imgur link leads us to: :ref:`GiLGL3S`
   
CONGLATURATION !!!
******************

In this video there is a small puzzle. Throughout the video Stratz uses text on screen in yellow. However every once in a while a single character will be tinted orange.
Putting all of these together forms 2vl73MwLqG8 which is a youtube link 🡫

.. video :: 2vl73MwLqG8

Explanation
===========

This video was an early little easter egg, these early secrets took place before the ARG and likely inspired Breadcrumbs to hide the ARG within Stratz's videos.