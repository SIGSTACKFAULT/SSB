###################
Land Speed Record 3
###################

.. video:: itr6vGHEYRE
   :frame: 2:14

2:14
****

.. image:: ../img/frames/land_speed_3.png
   :width: 45%
   
Stratish:
=========

*"The process cannot hurt me, but it can communicate and make me known. Its mind is weak, consumed with conviction. Perhaps it can be fooled."*

Rigel contemplating betraying Csrss for its own gain.

Link
====

Link leads to: :ref:`bjMFJaS`