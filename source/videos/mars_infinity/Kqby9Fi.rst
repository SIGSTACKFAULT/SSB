.. _Kqby9Fi:

#######
Kqby9Fi
#######

.. image:: ../../img/mars/Kqby9Fi.png
   :width: 45%
   
`Original Image <https://imgur.com/Kqby9Fi>`_

Stratish
********

| Top:
| Rigel: *For it was from your hand I was thrown into bramble*

.. image:: ../../img/mars/Kqby9Fi_s.png
   :width: 30%
   
| Bottom:
| Rigel: *Led to this horrible end*

.. image:: ../../img/mars/Kqby9Fi_s2.png
   :width: 30%
   
Corners
*******

In the corners you can read: :ref:`vBJKNuE`

Center
******

.. image:: ../../img/mars/Kqby9Fi_solution.png
   :width: 45%

By sorting the color's hex values from least to greatest, we get: :ref:`cVpGgwX`

Background
**********

The background is part of a larger puzzle here: :ref:`eTspnH2`