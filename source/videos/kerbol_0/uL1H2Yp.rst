.. _uL1H2Yp:

#######
uL1H2Yp
#######

.. image:: ../../img/kerbol_0/uL1H2Yp.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/uL1H2Yp.png>`_

Disclaimer: Download no longer works

Transcript
**********

::

 And before I forget - Rigel, I know you're reading this.
 You and your posse think you're so clever?
 We're coming for you.
 Godspeed.

Link
****

:ref:`LIGATURE`