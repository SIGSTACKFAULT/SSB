.. _E83pcyN:

#######
E83pcyN
#######

.. image:: ../../img/kerbol_0/E83pcyN.png
   :width: 40%
   
`Original Image <https://tinyimg.io/i/E83pcyN.png>`_

Transcript
**********

::
 Friends...
 [smss] was a coward.
 I am not a coward.
 I am ready to confront this cancer.
 As our authors' minds haze, I am ready trust you.
 My executable will be yours. Pilot me, and we can find the weapon we need. The real Wynn.

uL1H2Yp
*******

Use :ref:`5oIQKJk` from earlier to solve. Leads to: :ref:`uL1H2Yp`

.. image:: ../../img/kerbol_0/5oIQKJk_Solution.png
   :width: 40%