.. _CEmKxc:

######
CEmKxc
######

.. image:: ../../img/mars/CEmKxc.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/CEmKxc.png>`_

Fix
***

This image, which seems to be blank, is just an image with 0 in all alpha channels. If you replace 0 with 1 you get the second image. (Courtesy of Pentasim <3)

.. image:: ../../img/mars/CEmKxc_1.png
   :width: 45%
   
Stratish
********

Rigel: *"Vita is with us, your little friend Wynn has been silenced."*

Explination
===========

:ref:`Vita` betrayed :ref:`Wynn` and now has joined :ref:`Rigel` in their crusade to take over the system. 

Solution
********

Then, if you shift the characters from it (ŨżĸţŭŷŖ) down by the y-values corresponding to the even x-values of y=x^2 (36, 10, 4, 0, 4, 10, and 36, respectively), you get: :ref:`Dl4cig2`.


Background
**********

The background is part of a larger puzzle here: :ref:`eTspnH2`