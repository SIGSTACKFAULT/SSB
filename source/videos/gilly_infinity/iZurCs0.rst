.. _iZurCs0:

#######
iZurCs0
#######

.. image:: ../../img/gilly/iZurCs0.png
   :width: 45%
   
`Original Image <https://imgur.com/iZurCs0>`_

The third image.

Solution
********

| The sign is the overlay of 8, V and L.
| With the other two images :ref:`rQwoY8W` and :ref:`TCHCqDR` this leads to :ref:`Whaq8VL`.