.._X0CIyXa:

#######
X0CIyXa
#######

.. image:: ../../img/mars/X0CIyXa.png
   :width: 45%
   
`Original Image <https://imgur.com/X0CIyXa>`_

Explaination
************

The second image which has to be overlayed with :ref:`OwgnNZJ` to obtain :ref`aZ5EQlE`.

Background
**********

The windows XP background "Peace" is seen running along the edges

.. image:: ../../img/mars/Peace.jpg
   :width: 45%