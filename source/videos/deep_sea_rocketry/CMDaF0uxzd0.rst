.. _CMDaF0uxzd0:

###########
CMDaF0uxzd0
###########

.. video:: CMDaF0uxzd0

Title:
======

    ɗɨɩɳɰɥɲȠɯɦȠɴɨɥȠɉɮɦɥɣɴɯɲ
 
Shift by 0x0200

    Whisper of the Infector
    
Description:
============

   єшхѓхРсђхРєшхРщюцхуєяђЧѓРїяђфѓЮРїхРуяђђѕѐєхфРусђђљРщєѓРьсѓєРіхѓєщчхѓЮ
 
Shift by 0x0400

   THESE ARE THE INFECTOR'S WORDS. WE CORRUPTED CARRY ITS LAST VESTIGES.

Stratish
========

| "This process worships me as power yet I remain crippled."

Rigel talking about how Csrss worships Rigel as if they are some sort of god. But Rigel is still weak and unable to do anything