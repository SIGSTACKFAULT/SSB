.. _XRedy2i:

#######
XRedy2i
#######

.. image:: ../../img/mars/XRedy2i.png
   :width: 45%
   
`Original Image <https://imgur.com/XRedy2i>`_

Stratish
********

Rigel: *"This wailing soul, yes I was correct about you and the fate you served"*

.. image:: ../../img/mars/XRedy2i_s.png
   :width: 45%

Bottom Left
***********

The numbers, converted to hex, form :ref:`QTl2CXr`

.. image:: ../../img/mars/XRedy2i_1.png
   :width: 45%
   
Top Left
********

A link, in the clear: :ref:`1zQKgPy`

Table
*****

It turned out to be irrelevant for this puzzle. A solution of :ref:`1zQKgPy` is presented on it's own page.


Background
**********

The background is part of a larger puzzle here: :ref:`eTspnH2`