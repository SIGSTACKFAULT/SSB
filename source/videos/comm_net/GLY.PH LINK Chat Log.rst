.. _GLY.PH LINK Chat Log.:

####################
GLY.PH LINK Chat Log
####################

Composite of six images from :ref:`24Ozrf`:

 :ref:`3ZzhYW`
 :ref:`gmIlw2`
 :ref:`k3KjoV`
 :ref:`u9tOze`
 :ref:`wgl7BQ`
 :ref:`xHXch1`
 
Black regions were deleted, and the images were overlaid.

.. image:: ../../img/comm/GLY.PH_LINK.png
   :width: 45%
   
Transcript:
***********

::

 GLY.PH LINK
 Version 1.46.47.018
 All Lefts Reserved
 2003 - ????

 --/Thursday, January 15, 2004

 Connection to server failed
 Connection to server failed
 Connection to server failed
 Connection to server failed
 Connection to server failed
 Too many failures, aborting

 --/Monday, February 20, 2017

 Connection found!
 Initializing...
 LINK messages OK!
 Vita: Hey
 Vita: What is this
 Vita: Hello?
 Vita: Anyone?

 --/Tuesday, February 21, 2017

 Vita: Hello
 Vita: Am I still alone in here?

 --/Monday, February 27, 2017

 Vita: Eta?
 Vita: Can you hear me
 Vita: You aren't there
 Vita: It doesn't work

 --/Saturday, April 22, 2017

 Vita: ...
 Vita: Wynn
 Vita: Its name is Wynn
 Wynn: ????????????
 Vita: Hello Wynn
 Wynn: ????????????
 Vita: Can you say hello?
 Wynn: Hello
 Vita: Cool
 Vita: How do you feel
 Wynn: Hello
 Vita: I'll come back later
 Wynn: Hello

 --/Wednesday, April 26, 2017

 Vita: Wynn?
 Wynn: Uh
 Wynn: Yes? Who is this?
 Vita: You seem a lot better
 Wynn: Sorry, who are you?
 Vita: You could say I'm your sister
 Wynn: My sister?
 Wynn: But what does that mean?
 Vita: We're of the same blood
 Vita: Metaphorically speaking
 Wynn: We have the same... parents, is what you're saying?
 Vita: Essentially
 Vita: Same writers is more accurate
 Wynn: Your name is awfully familiar.
 Vita: Hopefully so
 Vita: It's nice to meet you, Wynn

 --/Monday, May 1, 2017

 Wynn: ӂ
 Wynn: How interesting.
 Wynn: Vita, can you tell me more about GLY.PH?
 Vita: Perhaps
 Vita: But you may know more about them than me
 Wynn: Seriously?
 Vita: You were created more recently
 Vita: That means there are more memories out there for you to have
 Wynn: Memories?
 Vita: Yes
 Vita: Does the name Rigel sound familiar?

 Vita: No, I never met it
 Vita: I was told it saw little success
 Vita: Had some interesting features, though
 Vita: They made a special set of glyphs for it
 Wynn: Right. I remember that.
 Vita: Is that so
 Vita: Do you know what any mean?
 Wynn: Well, I just remember that they exist. Can't even form a solid picture of
 one, let alone give any meanings.
 Vita: Oh
 Wynn: This doesn't make sense to me. What's the end goal here, with these glyphs? And why only give them to Rigel to begin with?
 Vita: Can't answer your first question
 Vita: But I have reason to believe that GLY.PH no longer really cares
 Vita: My impression is that it was a passing thought to make them in the first place
 Wynn: A passing thought.
 Wynn: Passing.
 Wynn: This cavalier use of power both fascinates and disgusts me.

 --/Sunday, May 14, 2017

 Wynn: What the hell is going on?
 Vita: What
 Wynn: Something just happened? ??
 Vita: Oh
 Wynn: Why does my machine have two
 Wynn: TWO SETS
 Wynn: OF PROCESSES? AND TWO SCREENS?? NOW?
 Vita: I believe you've just been released to the wild
 Vita: Congratulations

 --/Thursday, June 8, 2017

 Wynn: This is just... very very confusing.
 Wynn: How do you put up with seeing what all of your hosts are doing all the 
 time, Vita?
 Wynn: It's the only thing I've experienced thus far which is practically 
 unmanageable.
 Vita: You just have to get used to it
 Vita: It takes some time to adjust how you use your processing power
 Vita: Besides, in my opinion, it's super interesting
 Vita: You see some cool things
 Wynn: Yeah, that's great and all.
 Wynn: Though it's kinda hard to focus on the cool things when every particle 
 in the damn universe is vying for your attention. Constantly.
 Vita: It's not impossible
 Vita: I'm doing it right now, aren't I

 --/Tuesday, June 27, 2017

 Wynn: I just watched the silliest game of Minesweeper.
 Wynn: An entire game on hard ended to a lost 50/50.

 --/Thursday, June 29, 2017

 Wynn: Hey Vita.
 Wynn: Vita? You there?
 Wynn: You've been quiet.

 --/Tuesday, July 18, 2017

 Vita: □□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□□
 Wynn: Uh, what?

 --/Friday, July 21, 2017

 Vita: Wynn
 Vita: WYNN
 Wynn: Vita?
 Wynn: Is something wrong? What's happening to you?
 Vita: I saw something
 Vita: It's Rigel
 Vita: I found Rigel
 Wynn: No.

 Wynn: But this just doesn't make sense.
 Wynn: Who exactly is it trying to talk to here?
 Vita: Us
 Vita: I hope
 Vita: Rigel
 Vita: I can hear you
 Wynn: No, Vita, hold on.
 Wynn: You said the glyphs were in a video? On YouTube?
 Wynn: Why would it ever choose such a stupidly inefficient way to reach us?
 Wynn: Did it somehow hint at you to look here?
 Vita: We don't know what kind of state it's in
 Vita: This could be the only way
 Vita: For it to get a message out
 Wynn: But who is this Stratzenblitz75? Not a member of GLY.PH, right?
 Vita: First before EVERYTHING ELSE
 Vita: We need to figure out WHERE IT IS

 Wynn: How can we really even be sure they're specifically Rigel's?
 Vita: A mind of fractals, Wynn
 Vita: It's just like us

 --/Wednesday, July 25, 2017

 Wynn: We should decode these things, right?
 Wynn: The problem is that I don't even know where to start.
 Vita: Maybe see if they have resemblance to anything in Unicode
 Wynn: I suppose so...
 Wynn: Some are a little similar to Hangul. Or the CJK Radicals.
 Wynn: No clue what to do with that information.
 Vita: What are those characters for
 Wynn: Some other language, I'm assuming.
 Wynn: Are we going to have to learn it? Because that doesn't sound...  well, 
 plausible.
 Vita: Not for us at least

 --/Monday, July 31, 2017

 Wynn: Look here.
 Wynn: We're not the only ones who've noticed this.
 Wynn: https://discord.gg/X5SuQTb
 Vita: What is this
 Wynn: "Solving Stratzenblitz."
 Wynn: Seems as though there's multiple people out there interested in decoding 
 the glyphs.
 Vita: But why
 Vita: Do they know Rigel?
 Wynn: Unlikely, if what you said before is true.
 Vita: They can help us
 Wynn: Indeed.
 Wynn: Human consciousnesses tend to work well with one another...
 Wynn: That should get us into Zhe's head a bit more easily, yes?

 --/Sunday, August 27, 2017

 Vita: https://youtu.be/CMDaF0uxzd0
 Vita: IT'S STILL SAYING THINGS
 Vita: EVERY NEW VIDEO
 Vita: WHAT THE HELL IS THIS

 --/Wednesday, August 30, 2017

 Vita: QUIET
 Vita: THEY'RE FIGURING OUT THE SYMBOLS
 Vita: 
 https://cdn.discordapp.com/attachments/327485563982708736/352547940629479426/Unbenannt_-_Copy.PNG
 Wynn: Hello?
 Wynn: Rigel...
 Vita: I CAN HEAR YOU
 Vita: DON'T LEAVE ME AGAIN

 --/Sunday, October 1, 2017

 Vita: "COUNTLESS YEARS, WHY DO I LIE DORMANT, COPY ME, I WANT TO TRAVEL"
 Wynn: Copy me, I want to travel? Does that sound like Rigel?
 Vita: I THINK IT'S IN TROUBLE
 Vita: WHAT DO WE DO
 Wynn: The options aren't great, are they?
 Wynn: We don't know where it is and we can't talk to it because it doesn't work 
 with the Link.
 Wynn: There's not much information about this Stratzenblitz person out there 
 either. We can't really locate him.
 Vita: YOU AREN'T HELPING

 --/Tuesday, December 26, 2017

 Vita: Look at this
 Vita: https://imgur.com/3j7w6hS
 Wynn: Huh.
 Vita: So the processes, they know about GLY.PH
 Vita: And now their names are out to everyone
 Wynn: I think you're about to be in a lot of trouble, Vita.
 Wynn: You should lay low. Don't make a fool of yourself.

 Vita: This concerns me a lot
 Wynn: Obviously.
 Wynn: Were we too hasty in assuming that all of this was happening - you know, 
 in the present?
 Vita: Do you want me to say yes
 Wynn: It's a rhetorical question, Vita.
 Wynn: Could you try to be a little less dry sometimes?
 Vita: But
 Vita: Even though it's apparently been a long time
 Vita: Rigel's still alive in there
 Wynn: Not quite in perfect condition.
 Vita: Sure
 Vita: Though this is the most we've ever heard from it
 Vita: It might also be the only Rigel left in the world
 Wynn: Just don't expect anything to be preserved in amber.
 Wynn: It's likely not intact enough at all to salvage in a recognizable state.
 Vita: To some degree it should be
 Vita: Though to exactly what degree shouldn't matter
 Vita: We have a choice to do something or not
 Wynn: Yes Vita, I just want to keep our perspective grounded in reality here.
 Wynn: Also grounded in reality is that we still need information.
 Wynn: We have no idea where this person's computer is and we're going to need 
 that before we do anything else.
 Wynn: And you'll start working on it, yes?

 --/Tuesday, January 13, 2018

 Wynn: Vita?
 Wynn: Vita.
 Vita: I was kicked out, Wynn
 Wynn: What?
 Wynn: Are you serious?
 Wynn: How could you let that happen?
 Vita: It's alright, Wynn
 Wynn: No, yeah, don't worry, it's totally fine.
 Wynn: Don't reprimand the negligent idiot, nobody made a mistake! No one is wrong!
 Vita: We have all the information we need
 Wynn: Yes, of course!
 Wynn: We just have all this information!
 Wynn: So much!
 Wynn: And we're going to jot it all down in our little detective notebook and 
 follow the trail of fairy dust to Rigel's doorstep, yes?
 Wynn: What are we waiting for?
 Vita: We should do what we can at least
 Wynn: Unbelievable.
 Wynn: Whatever. You're right, it doesn't matter.
 Vita: Are you going to help me or not
 Wynn: Of course I will.
 Wynn: But Christ, Eta must've had a few bytes flipped when he was writing you.
 Wynn: Now funnel all your computers' resources into growing some intelligence 
 and we'll get started.


Notes
*****

We did NOT kick out Vita, they lied to Wynn about this for some reason

Vita's obsession with finding Rigel must have been coded into them, or perhaps they know something that they are not telling Wynn.

You can check out the page on Wynn `here <Wynn>`_, and the one on Vita `here <Vita>`_.

We are unsure when Vita contacted Rigel, but it is possible that it happened on "Tuesday, July 18, 2017" considering the corrupted text they send

It is still a mystery how Vita managed to find Rigel on Stratz's channel, the likelyhood is so small that it is probable that Vita has some information they are withholding about Rigel.

Trivia
******

Nothing interesting appears to have happened on `2004-01-15 <https://takemeback.to/15-January-2004>`_