.. _Building a Secret Base:

######################
Building a Secret Base
######################

.. video:: RKvr2lwx0lQ
   :frame: 0:50
   
====
0:50
====

.. image:: ../img/secret_base/RKvr2lwx0lQ_0m58s.png
   :width: 45%

The high-quality version if this image was given to us by a user named smss who joined the server, sent the image, and left.

.. image:: ../img/secret_base/smss.png
   :width: 45%

Top Base64
==========

Base64 decodes to ``REALIGNMENT REQUIRED``.

Bottom Shift
============

unicode, with a shift of 0x142. Decodes to ``tinyimg.io/???????.png``.

Middle
======

By changing every space to 5 wide, you get ↓

w35YYc0
*******

.. image:: ../img/secret_base/w35YYc0.png
   :width: 45%
   
`Original Image <https://imgur.com/w35YYc0>`_

The Cubes' RGB colors:

|#495049
|#315549
|#430000

↓ Convert from hex to ASCII; drop the trailing null bytes.


IPI1UIC
*******

.. image:: ../img/secret_base/IPI1UIC.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/IPI1UIC.png>`_
   
Stratish
========

Green Stratish
^^^^^^^^^^^^^^

Rigel (Presumably): There is no reply yet, Vita it has been released days what is the plan if Wynn never responds?

Red Stratish
^^^^^^^^^^^^

Vita (Presumably): We shall be, strain it by force Zhe and Eta will die if necessary

Explination:
^^^^^^^^^^^^

You may recognize the black hole in the center from the frame in :ref:`Stock Aircraft Carrier to Eve <Stock Aircraft Carrier>`, this is not a coincidence.

.. image:: ../img/frames/eve_carrier.png
   :width: 45%
   
June 21, 2017

The purpose of this is to show Vita's presence. It is also shown in the :ref:`GLY.PH LINK Chat Log <GLY.PH LINK Chat Log.>` that Vita found Rigel on the same day that this video released or slightly before

.. image:: ../img/secret_base/chat_log.png




