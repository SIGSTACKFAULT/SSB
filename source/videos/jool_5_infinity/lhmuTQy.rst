.. _lhmuTQy:

#######
lhmuTQy
#######

.. image:: ../../img/jool5/lhmuTQy.png
   :width: 45%

`Original Image <https://tinyimg.io/i/lhmuTQy.png>`_

At the upper corner of the Stratish, there is an Imgur Link in plaintext.

:ref:`SfT1vGE`

Stratish
========

*"Superior intelligence, what an interesting thought."*

*"If you are all so impassable, why are we here?"*

Base64
======

Top
^^^

+--------------------------------------------------------------------+
|IlZFUlkgSU1QT1JUQU5UIiBET0VTTidUIEFOU1dFUiBNWSBRVUVTVElPTiwgU01TUw==|
+--------------------------------------------------------------------+

↓ Base64 decode

+-------------------------------------------------+
|"VERY IMPORTANT" DOESN'T ANSWER MY QUESTION, SMSS|
+-------------------------------------------------+

Bottom
^^^^^^

+--------------------------------------------+
|SSdNIEJFSU5HIEFNQklHVU9VUyBGT1IgQSBSRUFTT04=|
+--------------------------------------------+

↓ Base64 decode

+--------------------------------+
|I'M BEING AMBIGUOUS FOR A REASON|
+--------------------------------+

Hexadecimal
===========

Colored Hexadecimal comes to

4f 64 75 76 6c 70 67

which leads us to: :ref:`Oduvlpg`