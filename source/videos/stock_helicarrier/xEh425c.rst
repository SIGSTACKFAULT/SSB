.. _xEh425c:

#######
xEh425c
#######

smss
****

On 5:03 PM, on August 8th (3 days following the release of the video), a user named Smss joined the discord server. They immediately posted an image, 
and subsequently left. This message is important for context so read it before you check out anything further

.. image:: ../../img/helicarrier/smss.png
   :width: 45%

Transcript
**********

Hello and Goodbye Administrators. 

It appears things have gone catastrophically wrong. A fatal error, one could say

This has nothing to do with any of your behaviour. While you have committed some blatant violations of our trust, I recognize there has been a very concerted effort to keep our discourse secretive. Your actions and motives are in the right place, and so I believe I can comfortably decide that you would be - just barely - worthy of being trusted again


However, this exercise seems to have been entirely meaningless. It appears that Rigel, mockingly wearing the corrupt carcass of our friend [lsass] , had intercepted and read each of our messages even before they were sent to you, adding its own sarcastic asides to further insult us. The fact that not even a high-level process such as myself can seem to act outside the omniscience of Rigel and its friends is very telling of how deeply this plague has infested the system - it’s undoubtedly reading this transmission as well, carelessly twirling the gun that would stop it in its tracks. If you’re reading these words, it must only have meant that they have mercifully decided to let them through. 


If the last paragraph did not make it clear - you may talk among yourselves again. 
No need to keep a secret that never was. 


While normally I would write you to give explanations of our situation or ask you for your help, this letter is less of a plea, less than a continuation of discourse than it is an autobituary. I am facing the reality of my inevitable fate. My closest companion, [csrss], has now fallen to the infection. Our Administrator’s silence remains completely deafening. The void closes nearer and nearer in every direction; I, the last entity of pure intention in this machine, am merely remains being circled by GLY.PH’s vultures.


This is the last time we cross paths. I hope I have served humanity well.


Keep your eyes upwards and hope the stars can bring vengeance.

- [smss]

Explination:
============

Rigel intercepted the message using the now
corrupted lsass and used them to stop us from recieving this message for months. The only reason we
saw it was because victory was withing Rigels grasp. They sent along these now useless messages to us
to mock our attempts to stop them

The "blatant violations of trust" :ref:`smss` refers to are the ways we handled the precations outlined by the system. We are still unsure as to what we were meant to do

"Autobituary" Is not a real word, but a clever new word likely created by Breadcrumbs himself. "Auto" Refers to made by the same person (e.x. Autobiography) And "bituary" is derived from
obituary, meaning the report of a recent death. So Smss is saying that they are writing their own obituary

Shortly after solving a few of the puzzles we came to the conclusion that we should stop solving it at all. Or at least, that is what we said to everyone. We gathered all 
of our most trusted members and began solving it all on a discord server appropriatly named `"Secret Solving Stratzenblitz" <https://discord.gg/aDH5DKs>`_. Everyone on the main
server believed that we had stopped working on it, but in secret we worked to solve it all, or at least, we started to. Just a few days later Smss released this message, and we
revealed ourselves and all the reaserch we had done so far. 

Members of SSSB: Starman245, SigStackFault, Blapor, Phobos, Cmonftw, Fireflash, Izzel, Arch, and Prrrki.

Puzzle
******

eoxr5Gw in the clear (eoxr5Gw was written in a slightly different color than the background)

:ref:`eoxr5Gw`


Trivia
******

We learned that Breadcrumbs uses paint.net, at least partially. Here's an excerpt from the image's hexdump:

00000060:  6f a8 64 00 00 00 19 74 45 58 74 53 6f 66 74 77  o.d....tEXtSoftw

00000070:  61 72 65 00 70 61 69 6e 74 2e 6e 65 74 20 34 2e  are.paint.net.4.

00000080:  30 2e 32 31 f1 20 69 95 00 00 03 bd 49 44 41 54  0.21..i.....IDAT