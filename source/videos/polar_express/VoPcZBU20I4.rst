.. _VoPcZBU20I4:

###########
VoPcZBU20I4
###########

.. video :: VoPcZBU20I4

Title
*****

+---------------------+
|ѣѡѬѭрѢѥѦѯѲѥрѴѨѥрѳѴѯѲѭ|
+---------------------+

By converting each character into hexadecimal and subtracting 0x420, we get

+---------------------+
|CALM BEFORE THE STORM|
+---------------------+

This likely refers to the war on the system soon to come. Soon after this Rigel will attempt to take over the system

Description
***********

+-----------------------------+
|ɨɴɴɰɳȺȯȯɰɡɳɴɥɢɩɮȮɣɯɭȯɊɹɦɇɖɔɎɧ|
+-----------------------------+

Like above, convert each character into hexadecimal and shift by 0x200. The result is

:ref:`JyfGVTNg`


Video
*****

0:20
====

Lines represent bianary. Left means 0, Right means 1. :ref:`FYSp3wh`

0:32
====

Instructions needed here

Hexidecimal, :ref:`MDmSf3o`

0:44
====

The third is a combination of binary and bitwise operations where the symbols above the boxes are the operation. 

There are 2 values, the white boxes and black boxes in the back. If you look closely when you use the bitwise operations 

to get the hexidecimal or decimal values these correspond to ascii letters which leads to this link. 

:ref:`3j7w6hS`

.. admonition:: TODO
   :class: danger

   MDmSf3o




