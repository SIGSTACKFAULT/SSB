.. _Eve Infinity:

############
Eve Infinity
############

.. video:: MTkObR6-Bs8
   :frame: 1:54

Thumbnail
---------

"Hello" in stratish

1:54
----

.. image:: ../img/frames/eve_infinity.png
   :width: 40% 
   
More of the language from Hyper Light Drifter.
To date, the language has never been used again.

| To see a world in a grain of sand
| and heaven in a wild flower
| hold infinity in the palm of your hand
| and eternity in an hour.

The link in the clear (now dead) is a joke.

Trivia
******

The background of the image is of the mission flag, distorted
