.. _nQEbIaZ:

#######
nQEbIaZ
#######

.. image:: ../../img/109_kiloton/nQEbIaZ.png
   :width: 60%
   
`Original Image <https://imgur.com/nQEbIaZ>`_

Table of Contents
*****************

+-+--------------------+
|1|`The Atlas`_        |
+-+--------------------+
|2|`Stratish`_         |
+-+--------------------+
|3|`Left Dots`_        |
+-+--------------------+
|4|`Bottom Characters`_|
+-+--------------------+
|5|`Right Hex Symbols`_|
+-+--------------------+
|6|`Background`_       |
+-+--------------------+

.. image:: ../../img/109_kiloton/nQEbIaZ_annotated.png
   :width: 60%
   
The Atlas
=========

For info on the Atlas check this page: :ref:`Stratish`

Stratish
========

.. image:: ../../img/109_kiloton/stratish.png
   :width: 30%
   
The top-left text translates to:

*"Your work is not complete"*

The middle text translates to "MDMSF"

This was intended as a hint explaining that :ref:`MDmSf3o` had not been fully solved at the time.

Left Dots
=========

.. image:: ../../img/109_kiloton/left_dots.png
   :width: 30%
   
Solved by Izzel on the discord server.

In each row, count the number of white dots and the number of red dots. 

These numbers represent two digits of a single hexadecimal number, with the number of white dots being the first (most significant) digit.

Do this for all eleven rows to get eleven hexadecimal numbers.

+--------------------------------+
|47 66 33 59 6c 72 4c 54 66 64 55|
+--------------------------------+

|Then, convert each number from hex to ASCII, resulting in Gf3YlrLTfdU.

:ref:`Gf3YlrLTfdU`

Bottom Characters
=================

.. image:: ../../img/109_kiloton/bot_characters.png
   :width: 30%

Solved by Pentasim on the discord server.

Look at the top string of characters. Find each character's hexadecimal representation in Unicode, 
then divide it by the index of that letter in the string. For instance, the first letter (l) is in the first spot, 
so divide its hexadecimal representation by 1 (or in other words, do nothing.) The second letter (ì) is in the second spot, 
so divide its hexadecimal representation by 2. Repeat for all seven characters.

6c ec fc 1d0 262 1ec 1dc

/1 /2 /3  /4  /5  /6  /7

6c 76 54  74  7a  52  44

Convert each quotient from hex to ASCII, resulting in lvTtzRd.

:ref:`lvTtzRd`

Right Hex Symbols
=================

.. image:: ../../img/109_kiloton/hex_symbols.png
   :width: 30%
   
Get the hex color codes of the two symbols. Every color is represented by three two-digit hexadecimal numbers, so this will give six hexadecimal numbers.

69 58 31 6e 31 6b

Convert these numbers into ASCII, resulting in iX1n1k.

:ref:`iX1n1k`

Background
==========
   
The background of this image is a corrupted version of the Windows XP stock background Wind. The use of Windows XP backgrounds throughout the ARG are to show that Rigel was created on a Windows XP computer

.. image:: ../../img/109_kiloton/wind.jpg
   :width: 30%
