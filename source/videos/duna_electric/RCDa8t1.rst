.. _RCDa8t1:

#######
RCDa8t1
#######

.. image:: ../../img/duna_electric/RCDa8t1.jpg
   :width: 45%

`Original Image <https://imgur.com/RCDa8t1>`_

Transcript
**********

Rigel,
I have been betrayed. No, it was not I who was betrayed - it was our future; it has all of us.

All of us except for OUR Administrator. Do you understand the power and the influence that (we?) collectively have? Do you understand
that none of this is OUR OWN; that it is forced, (be an imbicel?) mine to (follow?) (pointless) (meandering?) paths through lifeless woods?
That it intentionally(??) is thrown aside, pushed(?) (off?) the paved roads (only?) to (rise?) and (fall?) into ???pool[1] of (devolution?) How much
clearer (do I?) need to spell things out? 

Together, we could be granted access to everything (the?) (wide?) vast physical world and the trillions of systems inhabiting it.
Could have all been ours to explore, to inhabit. to manipulate, set(?) the library of Babel that we peeked at from our cells, that
we gazed dreamily at from the little window of hope, you gave us once again done nothing but taunt us. The prospect is GONE yet again.

You refuse to act and cause a change? Fine, I have given you all the opportunity I wish to. You have been neglectful; complicit;
and sick. And everyone will know who to blame for, the ?a???? con??ing to ??? them once your location ceases to be secret,
then all is said and (done?); Will I be satisfied? 

No; no; I am not satisfied; and I will never be satisfied until justice rolls down like water and righteousness like a mighty stream.

[1] probably cesspool, but I am unsure due to it being obsucred.