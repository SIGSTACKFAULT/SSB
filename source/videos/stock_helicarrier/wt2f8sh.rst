.. _wt2f8sh:

#######
wt2f8sh
#######

.. image:: ../../img/helicarrier/wt2f8sh.gif
   :width: 45%
   
`Original Image <https://tinyimg.io/i/wt2f8sh.gif>_

Solution
********

By overlaying all of the frames of the gif, you can read: :ref:`3NTMtrh`

.. image:: ../../img/helicarrier/wt2f8sh_1.png
   :width: 45%