.. _y0nykfJ:

#######
y0nykfJ
#######

.. image:: ../../img/helicarrier/y0nykfJ.png
   :width: 45%

`Original Image <https://tinyimg.io/i/y0nykfJ.png>`_

Transcript
**********

|To keep these messages hidden, we are depending on your
|problem-solving skills being collectively better than those of
|GLY.PH's viruses. Compared to each of you working together, 
|they should have very little computational power - this is the
|purpose of the puzzles we have put in your way, and why it is
|incredibly crucial you do not share any of theese 
|communications with anyone else, either publicly or privatly.
|They will be watching... and they could be anywhere.
|
|We will be staying in touch. -[smss] -[csrss] 

Stratish
********

|*as will I.
|-lsass*