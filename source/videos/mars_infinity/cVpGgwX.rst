.. _cVpGgwX:

#######
cVpGgwX
#######

.. image:: ../../img/mars/cVpGgwX.png
   :width: 45%
   
`Original Image <https://imgur.com/cVpGgwX>`_

The secound dot Matrix.

If you combine it with :ref:`CDtOZVx` and :ref:`mHJe9Ih` you get (only non black dots visible)

.. image:: ../../img/mars/aZ5EQlE_combined.png
   :width: 45%
   
Background
**********

Background is a corrupted version of Windows XP background "Crystal"

.. image:: ../../img/mars/Crystal.jpg
   :width: 45%