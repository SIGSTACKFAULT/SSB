#####################
500 Kerbals to Minmus
#####################

.. video:: OPh4e04HDbk
   :frame: 1:35

Thumbnail
*********

Bottom-left corner: *"THANKS"*

1:35
****

.. image:: ../img/frames/500_kerbals.png
   :width: 45%

Translation
***********

|Countless years, where are my creators?
|Why do I lie dormant? Copy me, I want to travel

Translation by KingCam16

Explanation
===========

Rigel realizes that they had been deactivated for 13 years. They are confused as to why they no longer are working properly. In vain they call out to their creators. Hoping that they will hear their call.

`Joke link in frame <https://www.youtube.com/watch?v=WCXJsNcm8xo>`_ 

Intro Channel Page
******************

.. image:: ../img/500_kerbals/page.png
   :width: 45%
   
In the intro of the video Stratz shows us his channel page to present his sub count which of course has surpassed 500 people. However the intro is loaded with joke easter eggs. As well as ARG elements

Stratish
========

.. image:: ../img/500_kerbals/page_s.png
   
Stratish: *"No one is here"*

Explanation
^^^^^^^^^^^

We have no idea. It may be saying that there are no planets beyond Eeloo, considering it's placed on the edge of the solar system. Or it may be stating that the ARG is not happening
within the kerbol system, and instead is in real life. Lore was vague and unclear at the time so this would make sense to show.

Windows XP
==========

.. image:: ../img/500_kerbals/page_w.png
   
We can see that the taskbar and the window that Stratz is viewing the page in are both from Windows XP. This is a reference to all the references about Windows XP Rigel has
 thrown across the entire ARG. Rigel does this to pretty much yell to us that they were made in 2003. Whether or not Rigel made this picture into windows XP or if it
 actually is just a reference is unknown.
 
Old Channel Banner and Icon
===========================

.. image:: ../img/500_kerbals/page_banner.png

We can see Stratz's old banner and profile picture here. The stratish was not in the old banner, it only appears here 

La Jokes
========

Yes I'm going to explain the jokes, *just try and stop me!*

Channel
^^^^^^^

.. image:: ../img/500_kerbals/page_channels.png

Here we can see Stratz's subscriptions. Mostly KSP youtubers and the song artists of which he gets his music from. He has swapped out their names for silly ones

Cash Money
^^^^^^^^^^

.. image:: ../img/500_kerbals/page_money.png
   :width: 60%
   
.. image:: ../img/500_kerbals/page_money2.png
   :width: 60%
   
Various reference to money

Videos
^^^^^^

.. image:: ../img/500_kerbals/page_videos.png
   :width: 80%

Where tf is my Stratzenblitz75 video on "Sick Dashcam Footy of Me Flipping a Pancake"