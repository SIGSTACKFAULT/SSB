.. _8bQXGVk:

#######
8bQXGVk
#######

.. image:: ../../img/helicarrier/8bQXGVk.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/8bQXGVk.png>`

Solution
********

The difference between each of the columns. For example, the first set goes 3, 21, 106. 
You would do 3 - 21 and 21 - 106, which equals -18 and - 85. Then, subtract -18 - (-85) = 67. 67 is C. Repeat for the rest of the columns and you get the link.

:ref:`Ca5VAj5`