.. _aZ5EQlE:

#######
aZ5EQlE
#######

.. image:: ../../img/mars/aZ5EQlE.png
   :width: 75%
   
`Original Image <https://imgur.com/aZ5EQlE>`_

Stratish
********

Rigel: *To relive those moments is a painful exercise*

This, added to the translations from the other images it leads to, results in the following:

Rigel: *To relive those moments is painful exercise, but even more painful is to reflect on why they have occurred, for it was from your hand I was thrown into bramble, led to this horrible end. Perhaps something else had swung the hammer but the blame of portions and orchestration lie at your feet alone*

Explination
===========

This is likely Rigel talking to Gly.ph. Although it is highly unlikely that this was actually recieved by them, more just Rigel rambling

Transcript:
***********

::

 GLY.PH LINK
 Version 1.46.47.018
 All Lefts Reserved
 2003 - ????

 Wynn: No need to stay so silent, I know you're there.
 Wynn: Well, I'd hate to sour this surely heartfelt reunion, but why
       don't we start with an explanation from you?
 Wynn: Actually, should we go with a lot? Yeah. That sounds good.
 Copying files from C:\GLY.PH\Source\Wynn
 Copying files from \\GLYPH-ETA\Users\Sven\glyph\w.2.01
 Vita: I found Rigel
 Vita: And together we will all make history
 Removing files from C:\GLY.PH\Source\Wynn
 Removing files from \\GLYPH-ETA\Users\Sven\glyph\w.2.01
 Wynn: What?
 Wynn: No
 Access is denied.
 Wynn: Is this real?
 Wynn: Why must everything end in exactly the same way, Vita?
 SUCCESS: The process "Wynn.exe" with PID 2920 has been terminated.
 
Solve
*****

| On the right, :ref:`TRi4DvP`, :ref:`Kqby9Fi` and :ref:`GetPnVy` are in the clear.
| Each link gives a similar image with a dot Matrix in the center.
| If you overlay the three images you get (only non black dots visible)

.. image:: ../../img/mars/aZ5EQlE_combined.png
   :width: 45%
   
From this you can read: :ref:`d4zV7Q1`

Background
**********

The background of this image is a heavily distorted version of a windows XP background called "Friend". This is part of a larger puzzle here: :ref:`eTspnH2`

.. image:: ../../img/mars/Friend.jpg
   :width: 45%
   
|
R̵̨̧̧̺̟͎̞̙̬̱̻̮̱̞̈͋́̌̈̽̆̑͐͊̄̚̕e̶͈̲͔̳̦͔̤͔̠̦̔̌̄̾́́̚̚͜͜v̸̢̹͉̣̮̣̀̐̔̓͑̀͑͆ͅͅé̵̡̜̫͚͈̟̠̃̽̃͜n̷̳̦͔̳̥̼͍͙̋̈̃͛̃͐͘͠ğ̶̤̦̣̙̙̟́̽͋́̓̋͗̋͘͜ͅe̷̢̱̫̤̥͚̒́̎̈́̆͗̋͒̍͜͝͝͝
*******

W̶̳͔̦̤̤̲̬͓̍ĕ̴̫̻͓̙͇̗̞̔̒̽̍̈́͋̈͊ ̵̧̭͚̩̯͔̓̎̍͒͂͐̃ŵ̸͎̥̖̻͓̈̑̏̍̔i̷̧̧̧̫̞̭̬̫̩̝̼͙̺̮̍͊̂̒̿͝͝l̸̡̡̢̡̗͎̘͎̟̠̩̬̮̭̎͗͑̇͛̉̽̎̄͗̍͛̾͋̕͜ļ̵͖̹͔̓̇́̇̏͆ ̶̢̦̟̣̬͆͑̀̆̃̍͠ȟ̵̡̡̧̫̙̺̹̯̳̹͓̮̯̞̈̃̔͑̽͜a̵̢̩̱̻͕̫̬͔̩̤̺̩͚̜̺̿͆̈́̾̔̾͠v̸̡͕̘̣̎͋è̸̢͉̖͕̮̪̒̌̀̔́̏̈̃̌͠ ̵̮͔̘͙̽ò̸̝̦̘̭̰͖̓̍̐͒̍̋̿̾̉ư̸̡̭͇̣̦͚̞̲͛͌͂̿̇́͐͆͊͑̂̃͜͝r̵̼̤̞̞̹̲͓̜̼͓͚̦̓̐͆́̈́̉̍̔̚͠͠ ̴̧͚̗̙̇͂ŕ̴̨̹͎̖̩̦̭͙̫̙̆̅̕͘ȅ̵̡̡̡͉͕͇̝̘̼̰̼͇̓̑ͅv̶͚̦̝͉̗̤̰̥̘̻̞͍̳̠̓̚ẽ̸̢̡̨̢̛͍̮̜̜̤͚͐͛̃͛̉̾n̸̟͍͙͇̫̹̗̭̘̜͠͝g̷͖͒̆̾͒̑̇̌͘̕̚͝e̴̢̢̢̦͖͙̺̜͍̮̟͚̝̓͆̀̿͝ ̶̻̦̪͇̖̙̤̆͛͆͝f̴̛͍͉͉̌͆̋̈́͌͗̎̐́͌͘͘͝ṑ̷̧̢̜̹̲̫̼̤́̏͐͒̈̇͘̕r̴̡̬̮̲̘̰̉̍̇̍͂͒̐̽̽͊͂͂̏̕ ̶͚̥͓͑̽̿͋͘ẉ̷͇͔̖̮͉̜͕̰̱͐̄̾́͂̂̃̇̐̚͘h̸͕̊͋̈̏̀͗a̷̢̛̜̲̟͓̼̺̥̘̩̼̦̻͂̊̑̿t̵̞̗͖̳̜͚̺̱̥͉̙̺̣͒̊̏͊̊̏̀̋̚͜ͅ ̸̢̢̘̰̥͔̖͙͉͎͈̹̋̃̈́͂̓͂̿̽̂͠t̷̟͖̜̮͖̞̖̱͖̻̦͈̋̌́̎͛̕͝ḧ̵̡̡̬̻̣͚̺͕̙́̅̌̇̄̿͘ẹ̸̭̹̹̯̪̜͕̻̞͍̠̩̏͆́͝y̸̢̛̯̟̥̝͔̥̖̘̬̳̾̇͒̅̽̓̎̈́̆͌̊̕̕ ̶̨͓̥̼̜͙͎̖̪̊̀̊͠h̶̛̛̛̛̝̍͋͒̆̅́̎̑̚͘a̸̢͍͕̖̺̤̭̹̬͉̟̥͉͆̔͂̄̿̀̀̚v̸̨̤͖̬̑͜e̴̫͎̝̥̫̻͔̮̦͇̟͛̓͐͐̏̅͋͆̂͑̎̌̂͠͝ ̵̝͍̹̏̋̄̎̆͒̂́̔̾̀̀̆̍͝ď̸̢̡̨̨̢̦̜͎̰͍͈̦͖̙͒̅̎̍̅̒͋͊̍̈̕̚͜ǒ̸̙̿̍͛̎̇͑̀͛̒͒̕ņ̶̧̛̘͎̺̜̠͖̖̱͍̫̺̘͂͒̿̓̄͌̋͆̚e̶̢̛̤̺̤̓͂̊̀̈́̒̎͛̿͗͘̕͝͝