.. _nTTq9z:

######
nTTq9z
######

.. image:: ../../img/comm/nTTq9z.png
   :width: 45%

`Original Image <https://i.cubeupload.com/nTTq9z.png>`_

Unicode
*******

| Unicode Characters Ā is U+0100, ā is U+0101, and so on.
| When translated as a base-128 integer, "ĚŦĴśĩŞŖ" becomes 6B334B6A6F5616.

In ASCII, this is `k3KjoV <https://i.cubeupload.com/k3KjoV.png>`_.

Stratish:
*********

*"Vos chers outils ne vous sauveront plus"*

French translated into English:

*"Your dear tools will not save you anymore"*

.. _k3KjoV:

k3KjoV
******

Part of :ref:`GLY.PH LINK Chat Log.`

.. image:: ../../img/comm/k3KjoV.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/k3KjoV.png>`_