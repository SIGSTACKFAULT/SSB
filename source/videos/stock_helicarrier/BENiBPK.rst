.. _BENiBPK:

#######
BENiBPK
#######

.. image:: ../../img/helicarrier/BENiBPK.gif
   :width: 45%

`Original Image <https://tinyimg.io/i/BENiBPK.gif>`_

Solution
********

Found by finding the position of the dots on each frame, calculating their velocity, then finding their acceleration, then converting acceleration values as decimal->ASCII.

Acceleration values
===================

+---+----+-----+
|Dec|Hex |ASCII|
+---+----+-----+
|51	|0x33|3    |
+---+----+-----+
|69	|0x45|E    |
+---+----+-----+
|77	|0x4D|M    |
+---+----+-----+
|81	|0x51|Q    |
+---+----+-----+
|87	|0x57|W    |
+---+----+-----+
|10 |0x64|d    |
+---+----+-----+
|88	|0x58|X    |
+---+----+-----+

Together, they form: :ref:`3EMQWdX`