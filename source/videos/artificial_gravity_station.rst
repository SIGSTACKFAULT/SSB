##########################
Artificial Gravity Station
##########################

.. video:: W4L2sIYR_3M
   :frame: 4:01

4:01
----

.. image:: ../img/frames/artificial_gravity.png
   :width: 45%

**Stratish:** 
*"My power is null, an influence I once had is corrupt. The consequences for disappointment are all but certain death."*

Rigel talking about how they are now weak, and that they believe Csrss's plan will end in their demise

The letters in the frame link to a lore text: :ref:`53o59gy`