.. _dTNunLk:

#######
dTNunLk
#######

.. image:: ../../img/mars/dTNunLk.png
   :width: 45%

`Original Image <https://imgur.com/dTNunLk>`_ 

There are 4 8-bit binary sections in the top half. Gray is equal to zero and Red is equal to one.

.. image:: ../../img/mars/dTNunLk2.png
   :width: 45%
   
In order from high to low, the binary strings read as follows:

+-----------------------------------+
|01001111 01110111 01100111 01101110|
+-----------------------------------+

This translates to "Owgn"

In the bottom right, there is a cube (which we have seen before) The hex code of the color is

+------+
|4E5A4A|
+------+

This can be translate to NZJ.

Put them together and you get :ref:`OwgnNZJ`