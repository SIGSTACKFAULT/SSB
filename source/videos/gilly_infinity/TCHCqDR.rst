.. _TCHCqDR:

#######
TCHCqDR
#######

.. image:: ../../img/gilly/TCHCqDR.png
   :width: 45%
   
`Original Image <https://imgur.com/TCHCqDR>`_

The second image.

Solution
********

The sign is the overlay of a and q.

With the other two images :ref:`rQwoY8W` and :ref:`iZurCs0` this leads to :ref:`Whaq8VL`.