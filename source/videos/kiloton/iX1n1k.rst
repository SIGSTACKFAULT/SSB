.. _iX1n1k:

######
iX1n1k
######

.. image:: ../../img/109_kiloton/iX1n1k.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/iX1n1k.png>`_

Transcript:
***********

::

 Doesn't one infection signify that there are many?

 Not always.

 I just... how can we be sure that [csrss] told us everything?
 If there is more than one virus on this machine, I really can't advocate for the plan anymore.

 How so?

 Our tomb becomes a trap.
 The beast becomes locked in with us.
 And if the Administrator comes back to save us, death will follow from the mausoleum.

 From: "Symantec Corporation" <security@symantec.com>
 Subject: Symantec: New Serious Virus found

 Norton Security response, has detected a new virus
 the Internet. For this reason we made this tool
 attachment, to protect your computer from this
 serious virus. Due to the number of submissions
 received from customers, Symantec Security Response
 has upgraded this threat to a Cagegory 5 (Maximum ).


 Prevention, using the W32.Gruel@mm Tool:
 To prevent or remove W32.W32.Gruel@mm, apply this
 attachment tool as quickly as possible. This is the
 easiest way to remove/prevent this threat.


 Technical Details:
 Also Known As: W32.Gruel@mm , W32.KillerGuate
 Type: Virus
 Infection Length: 45,195 bytes (zip file), 45,528
 bytes (executable) (45KB approx)
 Systems Affected: Windows 95, Windows 98, Windows NT,
 Windows 2000, Windows XP, Windows ME, Windows 2003
 Systems Not Affected: Macintosh, OS/2, UNIX, Linux


 Additional Information:
 Security response has received many submissions of
 corrupted W32.W32.Gruel@mm . A specific detection for
 this type of infected file has been added as
 W32.W32.Gruel@mm . This detection is available in
 virus definitions dated June 12 2003. Be sure to
 delete the files detected as W32.W32.Gruel@mm .


 Note: If you believe your computer may already be
 infected or just want to protect it agins
 W32.W32.Gruel@mm , please download this tool now.

 ymantec Corporation
 ast Updated on: July 13, 2003 04:44:35 PM

Explination
***********

As seen in :ref:`RCDa8t1` Csrss told the system of Rigels location. The system went ahead to delete it very soon after (shown in :ref:`AR9uTe6`). 
But, at the bottom of the page we see that it was not Rigel that was deleted, 
but Gruel Worm. Rigel must have either told Csrss a lie as to where they were located, or put Gurel Worm in their place while they ran away.
Gruel Worm likely came along with Rigel on the old hard drive

Notes:
******
For an unknown reason, the first letters of the last two lines are cut off. They would have been S and l.