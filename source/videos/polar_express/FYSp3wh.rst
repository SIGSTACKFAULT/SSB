.. _FYSp3wh:

#######
FYSp3wh
#######

.. image:: ../../img/polar/FYSp3wh.png
   :width: 45%
   
`Original Image <https://imgur.com/FYSp3wh>`_

This is a clue to :ref:`nmO0RL6` that eventually led us to finding a way to :ref:`ueBjsDsEGuA` and :ref:`8C4TZvF`