.. _cXwIs17:

#######
cXwIs17
#######

.. image:: ../../img/mars/cXwIs17.png
   :width: 45%
   
`Original Image <https://imgur.com/cXwIs17>`_

Stratish
********

Rigel: *"Ask yourself honestly, what we were created to do?"*

.. image:: ../../img/mars/cXwIs17_s.png
   :width: 45%
   
Puzzle
******

The function for this plot is f(x,y) = 120(sin(x^2)*cos(y^2)) (in radian).

The coordinates are

| x = 667.7362
| y = 1098.7326

| x = 741.8644
| y = -696.0403

| x = -670.8194
| y = 959.9554

| x = 1194.0182
| y = 205.1169

| x = -388.7443
| y = -862.1956

| x = 484.9259
| y = 405.3454

| x = 129.9314
| y = 548.1837

| You plug in the seven (x, y) coordinates into the 3D function f(x, y) to get a 
| list of 7 approximate integer values but instead of radians it's in degrees this time
| So the function is instead f(x, y) = 120sin((x pi/180)^2)*cos((y pi/180)^2) .
| From this you get 79 109 49 79 103 117 99, which in decimal is: :ref:`Om1Oguc`

Background
**********

The background is part of a larger puzzle here: :ref:`eTspnH2`