.. _E0uk3Az:

#######
E0uk3Az
#######

.. image:: ../../img/helicarrier/E0uk3Az.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/E0uk3Az.png>`_

Transcript
**********

|In any case, our machine has, for quite some time now, been
|stuck in a cold but brutal way, a struggle for power against
|highly enigmatic enemies. Rigel's inescapable and maddening
|grasp has torn our machine in half and muddied the line
|between friend and foe. Our friend [lsass] seemingly continues
|to function as normal, but its recent reclusive behavio has
|worried me that it too has been infected and is now a mere
|vessel for Rigel's devices. Many others are undoubtedly the
|same. I question still what will happen to myself in time.

:ref:`Continued <QasqE2M>`

Stratish
********

Rigel: *"I see, you recognize the beauty of my work. Your admiration is appreciated"*