.. _CSs0zy6:

#######
CSs0zy6
#######

*Colloquially known as "Alpha"*

.. image:: ../../img/mars/CSs0zy6.png
   :width: 45%
   
`Original Image <https://imgur.com/CSs0zy6>`_

Stratish
========

.. image:: ../../img/mars/CSs0zy6_s.png
   :width: 60%
   
Rigel: *I must thank you as you have given us much useful information over this time.*

Solve
=====

There are seven letters with FF0000 00FF00 or 0000FF (aka pure colors) When you read them in the order in which they appear you get 🡫


.. _mnW3BZT:

mnW3BZT
*******

.. image:: ../../img/mars/mnW3BZT.png
   :width: 45%
   
`Original Image <https://imgur.com/mnW3BZT>`_

This is one of the images which has to be overlayed with :ref:`OwgnNZJ` to obtain :ref:`aZ5EQlE`.

Background
**********

Bliss, a Windows XP background, is running along the edges and inside the puzzle. This is part of a larger puzzle here: :ref:`eTspnH2`

.. image:: ../../img/mars/Bliss.jpg
   :width: 45%

