.. _Gf3YlrLTfdU:

###########
Gf3YlrLTfdU
###########

.. video :: Gf3YlrLTfdU

Counting the number of white and red dots yields :ref:`EAeGxX0`

Title
*****

ՄՊՋգ՜Թդղմչց֎֏

All of the unicode values look like U+05XX, but a simple shift doesn't work. The description has the cipher algorithm. It translates to "DEATH FOLLOWS".

Description
***********

|ȰɸȵȰȰȠȫȠȰɸȵȠȪȠɩ

Shifting by 0x200 yields "0x500 + 0x5 * i," which we use to decode the title, with i being the position in the string.

Transcript:
***********

::
 ...This is insane.
 This is absolutely, totally, unapologetically insane.

 Yes, [taskmgr], that's the unfortunate truth
 But it's the only way we know of to completely ensure the safety of the data.

 I don't want to kill a process.
 I don't want to feel that pain.

 ...
 Look on the bright side, [taskmgr].
 Best case scenario, the administrator promptly replaces [csrss] and we can all get back to doing our jobs.

 Worst case?

 I suppose we just sit on a dead hard drive for eternity.
 We have nothing to lose, [taskmgr].
 This is what we were made for.

 I know.
 Let's save this machine.

Notes
*****

Conversation likely between Task Manager and lsass or smss. 
