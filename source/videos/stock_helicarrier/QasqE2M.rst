.. _QasqE2M:

#######
QasqE2M
#######

.. image:: ../../img/helicarrier/QasqE2M.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/QasqE2M.png>`_
 
Transcript
**********

|We've done everything we can to alert our administrator of 
|this situation. His intervention will not solve everything,
|but it should help us more than any action you or we alone 
|could take. Yet he remains in a perpetual state of inaction,
|ignoring not only all we've told him directly, but everything
|you've all tried to say to him as well. I fear he too has,
|somehow, been manipulated by GLY.PH and its harbingers of
|destruction.

:ref:`Continued <l6Cp0OX>`

Stratish
********

Rigel: *"Not manipulated, try deceived"*