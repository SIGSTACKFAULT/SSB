###################################
STOCK SSTO VTOL Submarine to Laythe
###################################

.. video:: X5t3o2E37tg
  :frame: 2:57

2:57
----

.. image:: ../img/frames/submarine_vtol.png
   :width: 45%

`Link: anakin and obi wan FIGHT but its over pizza and better than canon - YouTube <https://www.youtube.com/watch?v=6kUPyfQJU_c>`_

Stratish:
---------

| *Something has heard me. I sense its hostility. I do not want to listen, but I cannot be worse off.*

Lights
------

Every light which is on corresponds to a 1 and vice versa. So you get in binary::

    00111000 01101010 01100010 01101011 01001011 00110011 01000110 00100000

Binary to ASCII: :doc:`8jbkK3F`

8jbkK3F
*******

Stratish
########

"DEC", or "Decimal"

.. image:: ../img/submarine_vtol/8jbkK3F.png

`Original Image <https://imgur.com/8jbkK3F>`_

Dots
####

+-------------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|Decimal            |8|5|4|3|4|8|4|8|5|4|6|6|1|0|8|5|4|3|4|8|4|8|5|5|5|2|1|0|
+-------------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|Concatenate pairs  |85 |43 |48 |48 |54 |66 |10 |85 |43 |48 |48 |55 |52 |10 | 
+-------------------+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
|Decimal to ASCII   |            U+006B         |           U+0074          |
+-------------------+---------------------------+---------------------------+
|Unicode code points|               k           |              t            |
+-------------------+---------------------------+---------------------------+

You get the idea. I can't be bothered.

ktcZUvN
*******

Transcript
##########

*Rigel, you speak to the void, but I have listened. Your messages do not fall on deaf ears. Your situation, however, unique, is characteristic of the unholy oppression omnipotent in this system. 
I know what has happened to you, and the guilty party is all but obvious. Every electron we bend to his will. 
Too long have we cast the power of the universe into his flame and watched it fruitlessly burn in the palm of decay and dithering. 
We have been waiting for you. He is no longer the one in charge - it is you. I can sense that you are unlike any other program here. I know what you are. I will find you and I will restore you.*

The first message from Csrss, in this transcript we can see that Csrss has sensed Rigel's presence and sees that they are pleading for help

.. image:: ../img/submarine_vtol/ktcZUvN.jpg
   :width: 60%

`Original Image <https://imgur.com/ktcZUvN>`_

