.. _CDtOZVx:

#######
CDtOZVx
#######

.. image:: ../../img/mars/CDtOZVx.png
   :width: 45%
   
`Original Image <https://imgur.com/CDtOZVx>`_

The first dot Matrix.

If you combine it with :ref:`cVpGgwX` and :ref:`mHJe9Ih` you get (only non black dots visible)

.. image:: ../../img/mars/aZ5EQlE_combined.png
   :width: 45%
   
From this you can read :ref:`d4zV7Q1`

Background
**********

Background is a corrupted version of the windows XP background "Bliss" 

.. image:: ../../img/mars/Bliss.jpg
   :width: 45%