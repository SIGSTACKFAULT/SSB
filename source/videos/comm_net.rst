######################
Comm Network with Math
######################

.. video :: https://youtu.be/3Qb_gcJyGQI
   :frame: 15:16

15:16
*****

.. image:: ../img/frames/comm.png
   :width: 45%
   
Frame
=====

Text in frame: 

55200253112934₁₀ and 3234????????₁₆

| Convert 55200253112934 to hex: 32344F7A726616
| hex 32 34 4F 7A 72 66 = 24Ozrf in ASCII.
|
| :ref:`24Ozrf`