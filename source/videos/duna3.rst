.. _Duna³:

#####
Duna³
#####

.. video:: XuqWYfVc9MU
   :frame: 2:05

This is where the ARG canonically began.

2:05
****

.. image:: ../img/frames/duna_cubed.png
   :width: 40% 
   
| To see a World in a Grain of Sand
| And a Heaven in a Wild Flower,
| Hold Infinity in the palm of your hand
| And Eternity in an hour.


Translation by Exxion. Quote by William Blake.
The language used was from the Hyper Light Drifter ARG,
which would be dropped in favour of Stratish in :doc:`eve_infinity`.

Trivia
******

CONTAINS SPOILERS FOR HYPER LIGHT DRIFTER

The frame is slightly transparent so we can see the KSP UI still visible. The large crystal shape in the background is suspicously reminiscent of the Immortal Cell in Hyper Light Drifter.
This could be symbolism for the poem. The Immortal Cell was crafted by an ancient race that the HLD community refers to as the "librarians" they planned on creating this cell to make them
all immortal. This failed however, eventually causing the entire world to fall into disarray. The Immortal Cell could relate to the 3rd or 4th line.

.. image:: ../img/duna_cubed/cell.jpg
   :width: 40% 

