.. _mHJe9Ih:

#######
mHJe9Ih
#######

.. image:: ../../img/mars/mHJe9Ih.png
   :width: 45%

`Original Image <https://imgur.com/mHJe9Ih>`_

The third dot Matrix.

If you combine it with :ref:`VpGgwX` and :ref:`CDtOZVx` you get (only non black dots visible)

.. image:: ../../img/mars/aZ5EQlE_combined.png
   :width: 45%
   
From this you can read :ref:`d4zV7Q1`_

Background
**********

Background of this image is a highly corrupted version of the windows XP background "Follow"

.. image:: ../../img/mars/Follow.jpg
   :width: 45%