.. _GiLGL3S:

#######
GiLGL3S
#######

.. image:: ../img/duna_on_nerva/GiLGL3S.png
   :width: 45%

`Original Image <https://imgur.com/GiLGL3S>`_

Solution
********

The middle panel from the frame tells us what to do. Taking the color values and running them in order from hex into ASCII gives us 🡫

.. video :: eVLAQL70ByM

Totally worth it