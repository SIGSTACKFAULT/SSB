##########################
109 KILOTON Launch Vehicle
##########################

.. video :: SJWQoEMJEBw
   :frame: 2:48
   
2:48
****

.. image:: ../img/frames/109_kiloton.png
   :width: 45%
   
rFg3RMp
*******

.. image:: ../img/109_kiloton/rFg3RMp.png
   :width: 45%
   
`Original Image <https://imgur.com/rFg3RMp>`_

Each "strip" is a wallpaper from windows XP. The wallpapers each correspond to a hexadecimal number, 
determined by alphabetical order from A-Z. The first 2 numbers are already filled in. In order, they are:

+-------+---+
|Name   |Hex|
+-------+---+
|Friend |6  |
+-------+---+
|Ripple |e  |
+-------+---+
|Follow |5  |
+-------+---+
|Autumn |1  |
+-------+---+
|Crystal|4  |
+-------+---+
|Follow |5  |
+-------+---+
|Friend |6  |
+-------+---+
|Azul   |2  |
+-------+---+
|Crystal|4  |
+-------+---+
|Peace  |9  |
+-------+---+
|Friend |6  |
+-------+---+
|Autumn |1  |
+-------+---+
|Follow |5  |
+-------+---+
|Power  |a  |
+-------+---+

6e51456249615a

Converting from Hex to ASCII gives: :ref:`nQEbIaZ`

