################
Mini SSTO to Pol
################

.. video:: yEHIGfuyiTA
   :frame: 2:28

Thumbnail
*********

    Payload

2:28
****
.. image:: ../img/ssto_pol/Pol_Stratish.png

Translation
===========

    Lost eternally, fate torturous. Billy Gates, why do you make this possible,
    stop making money and fix your software!

Reference to the Blaster_ worm.

Youtube Link
============

Within the image there is a Youtube link.
The video can be found `here <https://www.youtube.com/watch?v=iPXKfGxeHIY>`_

.. _Blaster: https://en.wikipedia.org/wiki/Blaster_(computer_worm)

