###############
Jool 5 Infinity
###############

.. video :: LT2Fggr_LWQ
   :frame: 6:20

6:20
****

.. image:: ../img/frames/jool5.png
   :width: 45%

| This frame was actually a sequence of 3 frames
| Red characters lead to: :ref:`lhmuTQy`

Trivia
******

The flag for this mission was created by SigStackFault 

Because this mission had 3 frames it was noticed by many people, causing an influx of people on `SSB <https://discord.gg/X5SuQTb>`_