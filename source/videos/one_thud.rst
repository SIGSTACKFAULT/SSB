################
One Thud to Dres
################

.. video:: QJb3uWuiuT4
   :frame: 2:00


Thumbnail
---------

"Trapped". Rigel complaining about his fate.

2:00
----

.. image:: ../img/frames/one_thud.png
   :width: 45%
   
The link in the clear is a joke.

Direct :doc:`/stratish` translation:

    *Walls of the etarnal prison is a singular calling Akogu Tinia Krot*os says not today*

Apparently this translation was wrong.
We knew it when we saw qj8U-k1z4eA and found exactly the same pic near the end.

The right translation was:

    *Walls of is the prison eternal called singular. Kagou Anti KroSoft says not today.*
    
 "Kagou Anti KroSoft says not today" is a reference to `Kak worm <https://en.wikipedia.org/wiki/Kak_worm>`_
