.. _Muw5hr:

######
Muw5hr
######

.. image:: ../../img/comm/Muw5hr.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/Muw5hr.png>`_

| ordered from inner to outer circle
| long line is 0 and it goes in a clockwise rotation as depicted

Octal: 165 071 164 117 172 145

ASCII: u9tOze

.. _u9tOze:

u9tOze
******

Part of :ref:`GLY.PH LINK Chat Log.`

.. image:: ../../img/comm/u9tOze.png
   :width: 45%

`Original Image <https://i.cubeupload.com/u9tOze.png>`_