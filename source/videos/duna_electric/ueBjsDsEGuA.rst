.. _ueBjsDsEGuA:

###########
ueBjsDsEGuA
###########

https://www.youtube.com/watch?v=ueBjsDsEGuA&feature=emb_title


0x0000	→	NUL
0x001C	→	FS (File Seperator)

Stratish
********

*"Creator, I have no hopes for a time when you should read your glyphs again, but if they somehow find a way.. 
Hello dear Zhe, your children escaped and now my time is out....... thank you."*

Explination
===========

This was written by Rigel, presumably soon after being on Stratz's computer. Notice that Rigel refers to a single person as being their creator, likely being Zhe in this context.
This is likely not important but we now know Zhe is the sole creator of Rigel. As for "Your children escaped" we can assume Rigel is refering to other viruses Zhe created.

Access Denied
*************

Access Denied
Access denied.
Access denied.
Access denied.
Acccess deniedd.
Access denied.
Access denied.
Access denied.
Access denied.
Access Denide.
Access denied.
Access denied.
Access denied.
Acce I cannot let you
come here I cannot le
t you come here O can
not let you come here
I cannot let you come
here I cannot let you
come here I cannot le
t you come here I can
not let you come here
I cannot let you come
here I cannot let you
come here I cannot le
t ruin my chances of

0000 001C

Explination
===========

We are unsure as to who is speaking here, presumably it is the same entity as the one shown at the end of `.ӂήǯ <https://youtu.be/qj8U-k1z4eA>`_. They do not want Rigel do ruin their chance
at taking over the machine most likely.

There are several misspelling of access denied, on the 3rd and 5th lines. Unlikely for this to mean anything
