.. _Ulpf42:

######
Ulpf42
######

.. image:: ../../img/comm/Ulpf42.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/Ulpf42.png>`_

Base64
******

Bottom-left:


| aHR0cDovL3d3dy51bmljb2RlLm9yZy9jaGFydHMv 
| ↓ Base64 to ASCII

| http://www.unicode.org/charts/
| Grid
| Going by the existing characters, the first table is Cyrillic, the second table is Greek and Coptic, and the third table is Latin Extended-B

The character positions, in order, correspond to:

| U+0378 ͸
| U+0248 Ɉ
| U+0458 ј
| U+0463 ѣ
| U+0468 Ѩ
| U+0231 ȱ
| Take the low bytes as ASCII.

xHXch1

.. _xHXch1:

xHXch1
******

.. image:: ../../img/comm/xHXch1.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/xHXch1.png>`_

Part of :ref:`GLY.PH LINK Chat Log.`