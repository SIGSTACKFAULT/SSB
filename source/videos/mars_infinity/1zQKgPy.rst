.. _1zQKgPy:

#######
1zQKgPy
#######

.. image:: ../../img/mars/1zQKgPy.png
   :width: 45%

`Original Image <https://tinyimg.io/i/1zQKgPy.png>`_

It appears blank, but the bottom half is corrupted, as shown below. We know it is due to an extra 6 bytes in second png chunk.

.. image:: ../../img/mars/1zQKgPy_1.png
   :width: 45%
   
Solution
********

We have tried to use table from :ref:`XRedy2i` to find extra bytes, but it turned out to be irrelevant. Actually the useful table was in :ref:`figure_4.png` from the ligature folder download.

.. image:: ../../img/mars/figure_4.png
   :width: 45%
   
Go to 535th line of corrupted png chunk, there is 23 bytes in it. Green checkmarks in figure_4 show you values of bytes which you need to keep, 6 extra bytes, values of which are not shown in table, are the ones which you need to delete.

After deletion, the png file will provide link in clear: :ref:`c7jsCqN`

.. image:: ../../img/mars/1zQKgPy_2.png
   :width: 45%