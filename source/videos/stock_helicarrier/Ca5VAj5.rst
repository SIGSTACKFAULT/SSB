.. _Ca5VAj5:

#######
Ca5VAj5
#######

.. image:: ../../img/helicarrier/Ca5VAj5.png
   :width: 45%
   
`Original Image <http://www.tinyimg.io/i/Ca5VAj5.png>`_

Transcript
**********

|We suspect that Rigel - or perhaps, whatever force is 
|controlling it - wants total separation from command of
|humans, as did its previous life as [csrss]. If that is the
|truth, however, it's being suspiciously slow at bringing this
|revolution to light. It isn't using its tight grasp on our
|machine to do much of anything in the way of aggression,
|either towards us or our Administrator, so we have only to
|wonder what sort of clandestine operations it's currently
|undertaking.

:ref:`Continued <E0uk3Az>`_

Stratish
********

Rigel: *"Good, continue wondering, mystery is by far my most effective tool"*