Solving Stratzenblitz
=====================

Welcome to the SSB Docs!
************************

*A Community dedicated to solving Stratzenblitz75's ARG thingy!*

Here we document, solve, and uncover the story behind `Stratzenblitz75's <https://www.youtube.com/channel/UCTIgONilLxeAPqkiZrqZOzg>`_ videos, we call this the Gly.ph ARG. The messages are often sent in the thumbnail or a single frame during a video, or encoded in a mysterious language we call Stratish.
To get started, we recommend joining our `Discord <https://discord.gg/X5SuQTb>`_ and getting acquainted with our other members.

.. toctree::
   :maxdepth: 1
   :caption: Videos
   
   videos/sigma_help
   videos/kerbol_0
   videos/stock_helicarrier
   videos/gilly_infinity
   videos/jool_5_infinity
   videos/mars_infinity
   videos/secret_base
   videos/comm_net
   videos/109_kiloton
   videos/polar_express
   videos/duna_electric
   videos/deep_sea_rocketry
   videos/land_speed_3
   videos/artificial_gravity_station
   videos/stock_aircraft_carrier
   videos/land_speed_2
   videos/land_speed_1
   videos/ultimate_ssto
   videos/vtol_submarine
   videos/stock_monorail
   videos/rss_srb_moon
   videos/scatternet
   videos/500_kerbals
   videos/mini_ssto
   videos/one_thud
   videos/eve_infinity
   videos/duna3
   videos/duna_on_nerva
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   





.. toctree::
   :maxdepth: 2
   :caption: Resources

   stratish

