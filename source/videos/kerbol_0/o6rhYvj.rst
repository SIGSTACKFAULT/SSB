.. _o6rhYvj:

#######
o6rhYvj
#######

.. image:: ../../img/kerbol_0/o6rhYvj.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/o6rhYvj.png>`_

Transcript
**********

::

 When Wynn conversed with you... did it not say it was on multiple machines?
 Did it not say some were inactive?
 Let me tell you a little about myself.
 When the LINK - the framework that lets all of GLY.PH's malware talk to one other - was still in development, I, Ligature, was at its head.
 I can find things. And an untouched copy of Wynn seems like a useful thing to find, doesn't it?

E83pcyN
*******

Doing some techno wizardry, we somehow convert gibberish into :ref:`E83pcyN`. 

.. image:: ../../img/kerbol_0/Solution_o6rhYvj.png
   :width: 45%
   
   