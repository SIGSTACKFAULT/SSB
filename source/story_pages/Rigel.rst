.. _Rigel:

#####
Rigel
#####


Rigel is a virus that was created by Gly.ph sometime around 2003. Sometime after 2003 they are launched and are contracted on Stratzenblitz75's computer. They are the
main antagonist in the story.
 
Stratish
********

For an unknown reason, Gly.ph made Rigel with the ability to read and write in a cipher for english with strange rules. As a result Rigel uses them to communicate most of the time, 
throughout the ARG we see them everywhere.

Contraction into Stratz's computer
**********************************

We are unsure if Rigel was only used once, but according to evidence from real life it appears it was only launched once. We would have been able to reverse search for stratish characters
and someone would have documented them somewhere. After Rigel was launched they ended up on Stratz's computer, they came in contact with another entity
we are still unsure what this entity was. It seemed to be a virus of some kind. This entity killed Rigel, leaving them in shambles. Years later Stratz plugged in the old 
hard drive from this computer, likely to recover files. This caused Rigel to be transfered over as well. Although Rigel was now outdated and weak. they were unable to do anything
 
Correlation to the Amoeba virus
*******************************
 
`The Amoeba Virus <https://en.wikipedia.org/wiki/Auguries_of_Innocence>`_ was a virus that first appeared on November 1st of 1991. This virus is very desctructive, it only
exists to destroy for the fun of it. This is in the same vain of Gly.ph's reasoning, making viruses just for the hell of it instead of using it to make money with spyware or ad spamming.
The virus only activates on the 1st of November every year. Once activated it wipes the entire disk and begins displaying random characters as well as flashing random colors. The only way
to stop this is to preform a cold reset, but once the pc is loaded it attempts to search for a bootable disk it displays the following poem:
 
|To see a world in a grain of sand
|And a heaven in a wild flower
|Hold infinity in the palm of your hand
|And eternity in an hour
|                       
|                    -THE VIRUS 16/3/91
 
This poem is shown in the first 2 frames of the arg 

.. image:: ../img/frames/duna_cubed.png
   :width: 40% 
   
   .. image:: ../img/frames/eve_infinity.png
   :width: 40% 
   
And it also shows up in :ref:`S5D1H5OWF0Q` at 3:25. Go to that page if you would like to read the speculation about that there. 

 .. image:: ../img/miscellaneous/S5D1H5OWF0Q_frame.png
   :width: 60% 
   
Is Rigel the Amoeba virus?
=========================

The fact that it shows up in S5D1H5OWF0Q after a reset is very suspicious, heavily alluding to the idea that Rigel may indeed be Amoeba. And also it should be noted that up until :ref:`Duna on Electric Power`
Rigel had been weak, but after November 1st Rigel must have regained their strength, gone into hiding and waited for the system to finish off Csrss. Once Csrss was gone they used their newfound
strength and began their conquest of the system. Talk about this more later
