.. _dxWhqMJ:

#######
dxWhqMJ
#######

.. image:: ../../img/helicarrier/dxWhqMJ.png
   :width: 45%

`Original Image <https://tinyimg.io/i/dxWhqMJ.png>`_

Links
*****

+------+--------------+
|Top   |:ref:`HmTTDfp`|
+------+--------------+
|Left  |:ref:`BENiBPK`|
+------+--------------+
|Bottom|:ref:`wt2f8sh`|
+------+--------------+
|Right |:ref:`lMtiC4z`|
+------+--------------+
|Center|:ref:`dbiiita`|
+------+--------------+

Notes
*****

At the very top-center off the image, .gif is readable.

.. image:: ../../img/helicarrier/dxWhqMJ_1.png
   :width: 45%