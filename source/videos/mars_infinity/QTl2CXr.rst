.. _QTl2CXr:

#######
QTl2CXr
#######

.. image:: ../../mars/QTl2CXr.png
   :width: 45%
   
`Original Image <https://imgur.com/QTl2CXr>`_

Stratish
********

Rigel: *"How overzealous, as if we should let this continue"*

Rigel speaking about us, we know that they are speaking about us because they throw in a character created but SigStackFault. Rigel sent this so that they could make us aware that they were on the server, and would use it from now on

.. image:: ../../img/q.png
   :width: 45%