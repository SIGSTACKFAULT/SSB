##########################################
I want YOUR help with the Sigma Initiative
##########################################

.. video:: DcrkwxL3Mjg
   :frame: 1:38
   
1:38
----

 .. image:: ../img/sigma_help/IqQaorW.png
 
`IqQaorW <https://imgur.com/IqQaorW>`_ is an imgur link which leads to the
following.

Equation
--------
 
 .. image:: ../img/sigma_help/equation.png
 
It should be noted that this image looks a lot like yKzQ9l3.

.. admonition:: TODO
   :class: danger

   Add yKzQ913 and add link to it.
