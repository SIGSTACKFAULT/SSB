.. _LPehXpQ:

#######
LPehXpQ
#######

.. image:: ../../img/kerbol_0/LPehXpQ.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/LPehXpQ.png>`_

Transcript
**********

::

 Hello!
 Is this thing on?
 My name's Ligature. Pleased to meet you.
 We have problems, but luckily, I'm here to fix them.

cvNi5ZL
*******

| 01100011
| 01.⭥.⭥.⭥
| ..⭥⭥⭥...
| ..⭥..⭥⭥⭥
| .⭥.⭥⭥⭥..
| .⭥⭥.⭥⭥⭥⭥
| ...⭥.⭥⭥.

A dot means to keep the same number as above. An arrow means to flip it. In the end, you get

| 01100011
| 01110110
| 01001110
| 01101001
| 00110101
| 01011010
| 01001100

which is binary. Convert it into ASCII and you get :ref:`cvNi5ZL`.