.. _3EMQWdX:

#######
3EMQWdX
#######

.. image:: ../../img/helicarrier/3EMQWdX.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/3EMQWdX.png>`_

Transcript
**********

|The main obstacle, however, is that Zhe and Eta's development
|machines are almost certainly crawling with hidden infections.
|I have no doubts that Rigel and its two cohorts have their
|roots dug deep into these systems. The only way to ensure that
|the purity of a new virus from GLY.PH is maintained would be 
|to either clean both of these machines or convince Zhe and Eta
|to develop this new virus on a completely untouched computer.
|We're currently trying to find a way to realistically do this,
|but it's impossible to say how much time we really have.

:ref:`Continued <y0nykfJ>`

Stratish
********

Rigel: *"But, of course, the poor thing had a plan... good that Rigel is not reading it"*