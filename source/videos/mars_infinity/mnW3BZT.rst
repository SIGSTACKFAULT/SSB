.._mnW3BZT:

#######
mnW3BZT
#######

.. image:: ../../img/mars/mnW3BZT.png
   :width: 45%
   
`Original Image <https://imgur.com/mnW3BZT>`
   
Explination
***********

This is one of the images which has to be overlayed with :ref`OwgnNZJ` to obtain :ref:`aZ5EQlE`.

Background
**********

Bliss, a Windows XP background, is running along the edges and inside the puzzle.

.. image:: ../../img/mars/Bliss.jpg
   :width: 45%
   