.. _nmO0RL6:

#######
nmO0RL6
#######

.. image:: ../../img/duna_electric/nmO0RL6.png
   :width: 45%
   
`Original Image <https://imgur.com/nmO0RL6>`_

Table of contents
*****************

+-+-------------------------------+
|1|`Poem`_                        |
+-+-------------------------------+
|2|`Top Left Hex`_                |
+-+-------------------------------+
|3|`Bottom Left Coordinates`_     |
+-+-------------------------------+
|4|`Top Right Words`_             | 
+-+-------------------------------+
|5|`Top Right Bars`_              |
+-+-------------------------------+
|6|`Bottom Right Symbols`_        |
+-+-------------------------------+
|7|`Background`_                  |
+-+-------------------------------+

.. image:: ../../img/duna_electric/nmO0RL62.png
   :width: 45%

Poem
====

*"To create a little flower is the labor of ages."*

An excerpt from a poem written by William Blake called "Proverbs of Hell". This is the same writter of "Auguries of Innocence" which is prominently shown throughout the ARG, favoured by Csrss.
This poem was likely refering to Rigel, comparing them to a flower. Csrss planned on restoring Rigel over time to their former glory, which reflects "the labor of ages".

Top Left Hex
============

+----------------------------------------------------------------------+
|0x41A7 0x1E60 0x10 0x39AA400 0x157 0x100 0x157 0x400 0x4000 0x41A7 0x4|
+----------------------------------------------------------------------+

Convert the numbers from Hexadecimal to Decimal.

+----------------------------------------------------------------------+
|16807  7776    16  60466176  256    343   256   1024  16384  16807  4 |
+----------------------------------------------------------------------+

Then, find the power for each of those:

+----------------------------------------------------------------------+
|7^5    6^5     4^2   6^10    4^4    7^3    4^4   4^5   4^7   7^5  4^1 |
+----------------------------------------------------------------------+

Turn those into numbers:

+----------------------------------------------------------------------+
|75      65     42    6A      44     73      44    45    47    75   41 |
+----------------------------------------------------------------------+

Finally, convert the numbers, which are currently HEX, into ASCII.

+----------------------------------------------------------------------+
|u       e      B      j       D      s       D     E     G     u    A |
+----------------------------------------------------------------------+

Becoming a youtube link to a video called 0000 001C: :ref:`ueBjsDsEGuA`

Bottom Left Coordinates
***********************

From each of the crosses, find the X and Y coordinates from the bottom left edge. Add the two.

+---------------------+
|113 119 69 119 122 48|
+---------------------+

Then, convert from Decimal to ASCII

:ref:`qwEwz0`

Top Right Words
===============

Words
^^^^^

+----------------------------------------------------------------------------------------------------+
|BLISS MOON_FLOWER CRYSTAL BLISS BLISS CRYSTAL FOLLOW CRYSTAL FOLLOW POWER HOME FRIEND CRYSTAL FRIEND|
+----------------------------------------------------------------------------------------------------+

These are the names of Windows XP's default backgrounds. Using FYSp3wh found in Polar Express, we can assign each background a number. Take the original order in which they appeared in Windows XP and assign each a number, starting with 0. Count up in hexadecimal.

We can now assign the numbers to the above list.

+----------------------------------------------------------------------------------------------------+
|  3        8         4      3     3      4       5      4       5     A     7    6       4       6  |
+----------------------------------------------------------------------------------------------------+

Put the numbers into pairs of two:

+--+--+--+--+--+--+--+
|38|43|34|54|5A|76|46|
+--+--+--+--+--+--+--+

Then convert from HEX to ASCII

:ref:`8C4TZvF`

Top Right Bars
==============

Currently unknown

Bottom Right Symbols
====================

Read off the second two digits of each:

.. image:: ../../img/duna_electric/bot_right.jpg

+--------------------+
|61 33 41 4F 74 6C 4E|
+--------------------+

:ref:`a3AOtlN`

Background
==========

The background is a highly corrupted verson of the windows XP stock background "Radiance". Throughout the ARG windows XP backgrounds are used likely because Rigel was developed on Windows XP back in 2003.
This background may have been choosen due to the name of it. Radiance is sometimes used to refer to a higher being, for example you may refer to a god as radiant. This would further illustrate Csrss's obsessed worship of Rigel.

.. image:: ../../img/duna_electric/radiance.jpg
   :width: 45%

