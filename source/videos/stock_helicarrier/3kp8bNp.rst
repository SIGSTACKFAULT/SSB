.. _3kp8bNp:

#######
3kp8bNp
#######

.. image:: ../../img/helicarrier/3kp8bNp.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/3kp8bNp.png>`

Solution
********

|Found by Blackhole on the discord server.
|Something to do with converting colored arrays of dots into 7x7 matrices of pixels, 
then interpreting said matrices as 7 binary numbers. 
Send help, I'm still not completely sure how this one is done.

:ref:`Rk4HkEj`


.. admonition:: TODO
   :class: danger
   
   Ask Blackhole about this puzzle and complete the explination