########
Kerbol 0
########

.. video :: i60SXLaLI48
   :frame: 8:50

Video Explanation
****************

The first and last video in a new series on Stratz's channel called the Sigma Intiative. The Sigma Initiative would be succeeded by the `Upsilon Initiative <https://discord.gg/zNhcnZg>`_ later on, a community
project created by members from both `Solving Stratzenblitz <https://discord.gg/X5SuQTb>`_ and the `Official Stratzenbltiz Discord <https://discord.gg/BCYhkE7>`_.

8:50
****

Frame:

.. image:: ../img/frames/kerbol_0_frame.png
   :width: 60%
   
The letters 4PDQosD are made out of ASCII characters. The letters that make up the ASCII art lead to :ref:`PmKY2Zk`