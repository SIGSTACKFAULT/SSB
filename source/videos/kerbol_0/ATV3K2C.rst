.. _ATV3K2C:

#######
ATV3K2C
#######

.. image:: ../../img/kerbol_0/ATV3K2C.png
   :width: 45%

`Original Image <https://tinyimg.io/i/ATV3K2C.png>`_

Transcript
**********

::

 Rigel is weak. It's a decade and a half old, and just as delirious as it is outdated.
 This imbecile wants to rip the reins of technology from an entire world. How would you approach such a thing?
 Would you start assembling an "army" out of a useless guinea-pig worm and a reprogrammed bowling ball?
 No. It will never work. Unless, of course, we give them too much time...
 which is exactly what they want, and what they are starting to have.

o6rhYvj
*******

By subtracting The pixel mess from this image and :ref:`cvNi5ZL`, we get :ref:`o6rhYvj`

.. image:: ../../img/kerbol_0/o6rhYvj_lead.png
   :width: 45%