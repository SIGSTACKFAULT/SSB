.. _Om1Oguc:

#######
Om1Oguc
#######

.. image:: ../../img/mars/Om1Oguc.png
   :width: 45%

`Original Image <https://imgur.com/Om1Oguc>`_

Poem
****
|Where are the songs of spring? Ay, where are they?
|Think not of them, thou hast thy music too,-
|While barred clouds bloom the soft-dying day,
|And touch the stubble-plains with rosy hue;
|Then in a wailful choir the small gnats mourn
|Among the river sallows, borne aloft
|Or sinking as the light wind lives or dies;
|And full-grown lambs loud bleat from hilly bourn;
|Hedge-crickets sing; and now with treble soft
|The red-breat whistles from a garden-croft;
|And gathering swallows twitter in the skies.

Explination
===========

This is an excerpt from the poem `"To Autumn" <https://en.wikipedia.org/wiki/To_Autumn>`_ by John Keats. Specifically, it is the final stanza. This refers to the windows XP background "Autumn" it is part of a larger puzzle

.. admonition:: TODO
   :class: danger
   
   Background Puzzles
   
Text block
**********

¼ËßñÔ×¢Ëò«¥¼ì¸¥¬¯Þ®åæ³ú¨ÇÜË×±Ðë½
§ûÐ¬ÞúÖÎãòÚÅÉ§¨¡Ñó¸Úª²ÁÛîåÅÜÀêÕÇ
ººÕÒ¤Ýþ¦Ø¾Æü»À´ÄäõëåõÓªÅÓÐ×èª°Ùç
Ê¾äº¬È²ëçÇÅúèÜ¥º§²ÈáÆßÓ¦î±åìÇÓº®
ÉÂ½®¬¹ÝÌºÝ«ÛÌè×í¸íáôùõæ´÷÷ÀÌÒêõ¢
¥øÕÒû£áØ¡ÂÌ©Ïõ¡¨©èÀþÒÚóÙóú÷°ÉÛÕ¬
ÄèÞÑØâÕäÔòùþÆÒÅß¯Þ×ÜÑÏ÷¼÷Äæ¥Ð£´×
±Í¶ôäíÒ§Ø£ùô¢ÈÑÝï§ã¼¤¸ñ´ðùõôÐÍëì
Îñª÷ÍÕé÷üÔù£©·×ù§ôùÛó¹ÊíèèÒýªÏ§Á
ñ¥üíÍÃ¢ËðÛæ·¢õÞÕÉµ¥¢¾Ô¸ÇÞ§ÔçËÕùÐ
º¸ÏØ×¶Ùªþ±¦°¤õ§²Æ«âÚÈº£ñÕÜ®°æüàÈ
ñí·ùõ¢¾ÐÕ¢ÛæÉÞðÔÇ¸µÕ¢ËÔËÍüç§¥¥ÃÞ
õÛÔ¢ÔÃ¥ËÞ¥üÍñÕæðÉ·í¸¢ÇçùÞ§ÐË¢µÕ¾
Þ§ùíËÔÃüËÕÐÍ¾¢É¥Û¢ÇõÕµæð¥¢Ô¸ç·Þñ<
Õ§õ¸íæÇÐ¾µüÔÔðËÉñÍÛ¢¢¥ù¢·¥ÞÃçËÕÞ<
êéïÔÏ¡¿ò¨Çáý«ú®°ãô£îáèÏæÚÉ²ëÞ¼¸¾
¥ÍúíÉö»çàÚûµêöùßèü¶È®±¤ç²¸ãÝÌó±ø
æÕÞÔ¥¢µçÃÐÍÛ§ÉÞ¾ùËðÔ¸ÇüËõÕ¢·ñ¢¥í
Ùßû¶úÆÒäé¢ÔÇ¶ÅþÏÈþéî½É¬ëËöÀù£»æª
Çó¥Ò®û°õÒÈ¦öÄ³©¬ÑåâæçîÀ¤ÞìÓ¡ãÕäÄ
¼È·¦ÓØ¾ºÞ°Ä·ÑëãàÐø³¦¹é³Üàí¿äñâã¾
«ÎÍúñÁ¢áñ¨üòëâ¡ìÁû¡ö×X<-(12,12)¬
æÛÈÔ«âã§Ñâµçþõëùô£üÊõÛÈÞ¢Ñ¨õ¸À±á
Þ¾ÇÃñ¥ùæÛÐ§ÕË¸ËÉ¢ÍÔõ·Õí¢Ôµð¥¢Þçü
¯âë½²ÆâÚÒ¸ËÆÍ°ÐçóÆ¢ÙâÌÂ³Êóþ²ãëÂ¼
¸Ô³ÕõæþÞ÷·þÀïÛÃÜ´Ù¹¥ã´òÃë¤åªá¢ûà
ãÈÈ®è·÷§Ýõ«»õÇøÜÝÁçÒØ¬£ÓºãüÎîÌµ°
ÆÖê¿ØµÓä¦îäøÑÏâË¬æâØÆ¼ìè¦ÈºÙò»ùÎ
ÔÄ¦ñ·¹àøÕ×²Î¤÷È³ëÈ¹ìü¤Ëôó¹²èÎê£Ê
¹ßÅ±Ò¦¦òõç¾èä»Æàú©¨Îø¤¹ÉÛçàÎ²Ù¦Ä
ÆÎÕ¥èÅ¼Ü×÷¨îÏÑÅ¹³âìµý°¾ÜÊÕ·³õÒ¡ä
×¢ûÄÞº¡ëÖñÍøãùåè½®ÞßÒ±÷¦ÉòÌéÂ¸Öå

The arrows are colored #ff8c11 and #ff0000. The only byte in ASCII range (11) is the non-printable character DC1 (XON)