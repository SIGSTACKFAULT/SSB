.. _AH93m6T:

#######
AH93m6T
#######

.. image:: ../../img/gilly/AH93m6T.png
   :width: 45%
   
`Original Image <https://imgur.com/AH93m6T>`_

Transcript
**********

*"I just don't understand why they keep sending these messages, It can only be harmful to their cause, right?"*

Explination
===========

Likely spoken by one of the system programs. They are confused as to why Rigel is still sending out the frames to us, and Rigel mocks them by putting this very message into the frame.

Puzzles
*******

Bottom
======

:ref:`YlBSpa2` in the clear.

Top
===

 If you stack the shape on top of itself you can read :ref:`rQwoY8W`

.. image:: ../../img/gilly/top.png
   :width: 45%

Cubes
=====

Hex colors: #ABBCB7, #BC8EBB, #ADFFFF

If you invert the colors you get 54 43 48 43 71 44 52, ignoring the last two FF.

This translates to :ref:`TCHCqDR`

Bottom Left
===========

To solve the bottom left corner of AH93m6T, use the clues from P5tQ1pX. The center of P5tQ1pX describes the encryption method used:

+---------------------------------------------------------------------------------------------------+
|Step 1: if it's red, subtract 12                                                                   |
|Step 2: if it's green, multiply by 4                                                               |
|Step 3: if it's blue, add whatever you have at this point to itself (or equivalently multiply by 2)|
+---------------------------------------------------------------------------------------------------+

To decrypt iNêƘČΘĠ, convert them to decimal and reverse the process for each character using the color above it:


+-------+---------+----------+---------+
|Color  |Encrypted|Decryption|Decrypted|
+-------+----+----+--+---+---+---+-----+
|       |char| #  |/2| /4|+12| # |char |
+-------+----+----+--+---+---+---+-----+
|#000000|i	 |105 |  |   |   |105|  i  |
+-------+----+----+--+---+---+---+-----+
|#FF0000|N   |78  |  |   | • |90 |  Z  |
+-------+----+----+--+---+---+---+-----+
|#0000FF|ê   |234 |• |   |   |117|  u  |
+-------+----+----+--+---+---+---+-----+
|#FFFF00|ƙ   |408 |  | • | • |114|  r  |
+-------+----+----+--+---+---+---+-----+
|#00FF00|Č   |268 |  | • |   |67 |  C  |
+-------+----+----+--+---+---+---+-----+
|#00FFFF|Θ   |920 |• | • |   |115|  s  |
+-------+----+----+--+---+---+---+-----+
|#FFFFFF|Ġ   |288 |• | • | • |48 |  0  |
+-------+----+----+--+---+---+---+-----+

This results in :ref:`iZurCs0`

Combination
***********

Each corner of the triangle gives a similar image in which you have to find characters which when overlayed result in a given symbol.

The top gives Wh.

The bottom right gives aq.

The bottom left gives 8VL.

Together this forms: :ref:`Whaq8VL`
