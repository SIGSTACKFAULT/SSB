.. _MDmSf3o:

#######
MDmSf3o
#######

.. image:: ../../img/polar/MDmSf3o.png
   :width: 45%

`Original Image <https://imgur.com/MDmSf3o>`_

Status: TODO

Explanation TODO

Center
******

Follow the numbers written in cells of (MDmSf3o) from 1 to 7. Write all of their coordinates (first coordinate - number of cells in short row from the left (begins with zero), second coordinate - number of cells in long row from the above (also begins with zero)) in row. Convert those into ASCII. The result is

:ref:`faot6yB`

Bottom
******

TODO AR9uTe6

.. admonition:: TODO
   :class: danger

   EVERYTHING