####################
Stock Parts Monorail
####################

.. video:: svhHcHdPgeA
   :frame: 2:02

2:02
****

.. image:: ../img/frames/monorail.png
   :width: 45%
   
Stratish:
=========

*"I wait in scilence, somehow my fear seems empty"*

`Joke link in frame <https://www.youtube.com/watch?v=l_3Nhbn1Mx8>`_

    *You have any idea how hard it is to drive a vehicle across ice,*
    *nevermind one with almost no grip and you can't steer,*
    *only control the throttle?* -- SIGSTKFLT, YouTube comment


