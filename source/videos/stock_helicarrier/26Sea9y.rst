.. _26Sea9y:

#######
26Sea9y
#######

.. image:: ../../img/helicarrier/26Sea9y.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/26Sea9y.png>`_

Solution
********

Table is solved by adding each pair of numbers across and placing the sum under the second number. Then, last column ->hex -> ascii

:ref:`SQcNl4F`