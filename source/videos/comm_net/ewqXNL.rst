.. _ewqXNL:

######
ewqXNL
######

.. image:: ../../img/comm/ewqXNL.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/ewqXNL.png>`_

Solve
*****

a is the amplitude and b is the phase shift.

Hex: 33 5a 7a 68 59 57

ASCII: 3ZzhYW

.. _3ZzhYW:

3ZzhYW
******

Part of :ref:`GLY.PH LINK Chat Log.`


.. image:: ../../img/comm/3ZzhYW.png
   :width: 45%
   
`Original Image <https://i.cubeupload.com/3ZzhYW.png>`_

