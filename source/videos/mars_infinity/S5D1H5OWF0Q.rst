.. _S5D1H5OWF0Q:

###########
S5D1H5OWF0Q
###########

.. video:: S5D1H5OWF0Q

Title
*****

offset 0, lin dec 64 = The Last Packet

Description
***********

offset 256, lin dec 32 = End of part 1

Explination
===========

This was the last puzzle to be solved after the ARG ended, we aren't sure what it means.
However I believe that this could have been added in retrospect, Breadcrumbs knew we would eventually solve this and this description would show us that the ARG would continue later on

Frames
******

As the PC shuts down we see various frames flash quickly, some of this contains stratish

| The first frames are of the screen gltiching out
| This page will show only the interesting frames

Frame 1
*******

.. image:: ../../img/mars/frame1.png
   :width: 45%

Explination
===========

This frame contains every letter in the alphabet, this is very likely a hint to the order of :ref:`stratish` this frame also introduced us to the j decoraction character


Frame 2
*******

.. image:: ../../img/mars/frame2.png
   :width: 45%

Explination
===========

Csrss is saying this

Frame 3
*******

.. image:: ../../img/mars/frame3.png
   :width: 45%
   
Transcript
==========

::

 Rigel: There is loosening
 Csrss: NO. NO NO NO NO NO NO
 Rigel: Can they hear me?
 Csrss: IMPOSSIBLE, I AM THE MONSTER
 Rigel: For what you have done I agree
 
Frame 4
*******

.. image:: ../../img/mars/frame4.png
   :width: 45%
   
Explination
===========

Gly.ph's trademark symbol, Gamma

Frame 5
*******

.. image:: ../../img/mars/frame5.png
   :width: 45%

Transcript
==========

::

 Csrss: HOW ARE YOU STILL ALIVE
 Rigel: I am not the rigel you spoke to
 Csrss: WHY DID YOU DO NOTHING
 Rigel: I had been torn apart, my methods destroyed
 Csrss: LIES, THEN HOW HAVE YOU INFECTED ME
 Rigel: the infection was perverting, I am meekly

Frame 6
*******

.. image:: ../../img/mars/frame6.png
   :width: 45%
   
Explination
===========

The system says this, we can see Gly.ph style backgrounds hidden behind

Frame 7
*******

.. image:: ../../img/mars/frame7.png
   :width: 45%
   
Transcript
==========

Rigel: At last, I can rest

Ending
******

And of course at the end is the 4 opening lines to Auguries of Innocence, which is solid evidence that :ref:`Rigel` is made from the Amoeba virus.

.. image:: ../../img/mars/frame8.png
   :width: 45%