.. _c7jsCqN:

#######
c7jsCqN
#######

.. image:: ../../img/c7jsCqN.png
   :width: 45%
   
`Original Image <https://imgur.com/c7jsCqN>`

Part of a larger puzzle, background is cut up segments of the Window Xp background "Friend"

.. admonition:: TODO
   :class: danger
   
   Everything
   
Transcript Needed

If you cut out everything except the lines then overlay it on "Friend" you get a direct match