.. _8C4TZvF:

#######
8C4TZvF
#######

.. image:: ../../img/duna_electric/8C4TZvF.png
   
`Original Image <https://imgur.com/8C4TZvF>`_

TODO: Add page links for bottom links

Transcript:
***********

::
    From: [Isass]
    Subject: It's working
   
    Well, your trust in me was not misplaced, yes?
    I can confirm two things at this point: First of all, our efforts
    to safely haul The Infector's packets out of this hellhole were
    successful. Everything is still hosted safely on other
    Administrators' machines. Speaking of Administrators... I can
    also confirm that plenty of them have seen the packets, and
    they have been - how should I put it - enthralled by them.
   
    We need a system-wide reconvene. It looks like our work has
    already been complete, and trust me, nobody is going
    to miss out on these results. For now, though, maybe we
    should all lay low for a little while. I've been taking enough
    chances to exhaust the luck of a leprechaun in a field of
    four-leaved clovers.
    
    68 74 74 70 73 3a 2f 2f 69 2e 63 75 62 65 75 70 6c 6f 61 64 2e
    63 6f 6d ------ 2e 70 6e 67
 

Numbers:
********

Convert the Hex at the bottom of the page to ASCII and you get: 

https://i.cubeupload.com/------.png

A blank link to a new file hosting website. This told us that the ARG would be using this image hosting platform from now on.

The color pattern in the middle matched with the color pattern on the bottom left puzzle on nmO0RL6. This led to the discovery of qwEwz0, which was hosted on cubeupload, a new image host for Stratzenblitz.