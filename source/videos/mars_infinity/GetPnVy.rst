.. _GetPnVy:

#######
GetPnVy
#######

.. image:: ../../img/mars/GetPnVy.png
   :width: 45%
   
`Original Image <https://imgur.com/GetPnVy>`_

Stratish
********

Rigel: *"Perhaps something else had swung the hammer, but the blame of portions and orchestration lie at your feet alone."*

Function Equation
*****************

The function equation is f(x) = 0.1 x^2 cos(x) sin(0.2 x)^4 (courtesy of RainbowUnicorn) plugging the x into it gave us: :ref:`mHJe9Ih`

Hexadecimal in Top Right
************************

43 70 51 6e 30 62 51

translates to: :ref:`CpQn0bQ`

Background
**********

This is part of a larger puzzle here: :ref:`eTspnH2`