.. _d4zV7Q1:

#######
d4zV7Q1
#######

.. image:: ../../img/mars/d4zV7Q1.png
   :width: 45%

`Original Image <https://imgur.com/d4zV7Q1>`_

Four imgur links are in the clear, three finally lead to similar images with a rectangle with ANSI characters.

Theta: :ref:`cXwIs17`

Lambda: :ref:`YIVUAIH`

Iota: :ref:`XRedy2i`

Kappa: :ref:`CEmKxc`

Transcript
**********

| *...Do you know what the other processes lack?*
| *The ability to think critically. To solve problems*

Rigel speaking to us

Solution
********

After solving all 4 puzzles, you will get 4 "text walls". You should overlap them, as shown in the image below, so that X<-(12,12) symbols will be on top of each other.

.. image:: ../../img/mars/pack.png
   :width: 45%

7 letters É will be overlapped too, just like X<-(12,12). Assume X as a letter with coordinates (12, 12), and find coordinates of all É-s. Transcibe them into HEX and then into ASCII:

+-----------+---+-----+
|Coordinates|Hex|Ascii|
+-----------+---+-----+
|(7, 0)     |70 |p    |
+-----------+---+-----+
|(3, 2)	    |32	|2    |
+-----------+---+-----+
|(7, 3)	    |73	|s    |
+-----------+---+-----+
|(5, 4)	    |54	|T    |
+-----------+---+-----+
|(6, 5)	    |65	|e    |
+-----------+---+-----+
|(4, 8)	    |48	|H    |
+-----------+---+-----+
|(6, 14)	|6E	|n    |
+-----------+---+-----+



Resulting letters (p2sTeHn) need to be sorted out, so this is where hue of the arrows will help. Using hue (in ascending order), we can finally sort these letters into right order: :ref:`eTspnH2`

.. image:: ../../img/mars/overlap.png
   :width: 45%