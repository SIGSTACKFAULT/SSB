.. _SQcNl4F:

#######
SQcNl4F
#######

.. image:: ../../img/helicarrier/SQcNl4F.png
   :width: 45%

`Original Image <https://tinyimg.io/i/SQcNl4F.png>`_

Transcript
**********

Hello Administrators,

I am [smss]. Helping me to bring you these messages is my 
companion [csrss]. Don't be alarmed - this is a completely
different being than our old villain, the Rigel-infested
subsystem that existed on GAME. That one is long dead by this point.

:ref:`Continued <Rk4HkEj>`

Stratish
********

Doing this dance again, I see. I will let them read your monologue this time.