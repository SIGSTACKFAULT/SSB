.. _qj8U-k1z4eA:

###########
qj8U-k1z4eA
###########

.. video :: qj8U-k1z4eA

Title
*****

::
 .ӂήǯ
 The title has 3 characters, representing members of GLY.PH.

 ӂ = zhe.breve
 ή = eta.tonos
 ǯ = ezh.caron

Description
***********

D85 69BCD @13;5D

ASCII Shift + 16 results in

THE FIRST PACKET

Stratish
********

*Courtesy of Rainbowunicorn and Phobos*

::
 writing registry key to “HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run”

 dropping self to C:\Windows\System32\oobe
 beginning infections
 status set to busy

 infecting C:\Windows\System32\cmd.exe

 Kagou Anti KroSoft says not today

 infecting C:\Windows\System32\rwinsta.exe

 infecting C:\Windows\System32\Narrator.exe

 infecting C:\Windows\System32\ipv6.exe

 infections aborted: manual termination
 status set to idle

 shutting down

 thank you

.. image:: ../../img/helicarrier/thanks.png
   :width: 45%

 Walls of is the prison eternal called singular
 Kagou Anti KroSoft says not today

.. image:: ../../img/helicarrier/first.png
   :width: 45%

Explination
===========

Back in 2003 Rigel was launched, they somehow landed on Stratz's old computer. But, upon entering the PC they encountered a strange program, likely another virus that had already infected
Stratz's computer. Rigel was terminated. 13 years later Stratz would plug in his old hard drive to retrive old files or something of the like. In doing so Rigel transfered along with the
files. Outdated, broken and weak. We see the first ever Stratish frame we ever saw put in on the final scene of this video.

Notes
*****

This video introduced us to many new :ref:`Stratish` characters. Including Periods, Colons, Quotations, Vowel Replacements, slashes and the numbers (2, 3 and 4).

Technical explanations
**********************

|oobe - Out-of-box-experience folder, it has some files needed for the first start of OS 
|cmd.exe - Command prompt. If you have an access to cmd.exe and (optionally) admin rights, then you have access to pretty much everything on this PC 
|rwinsta.exe - small program for closing offending/exceeding sessions on remote desktop panel 
|Narrator.exe - special text-to-speech program 
|ipv6.exe - setup for IPv6 connections