##############
Gilly Infinity
##############

.. video:: vb4-yAPC35U
   :frame: 4:10
 
4:10
****

.. image:: ../img/frames/gilly.png
   :width: 45%
   
Count the edges on each shape (dot considered as 1), converted to hex:
   
.. image:: ../img/gilly/frame_1.png
   :width: 45%
   
+--------------------+
|41 48 39 33 6D 36 54|
+--------------------+

↓ Hex to ascii

:ref:`AH93m6T`