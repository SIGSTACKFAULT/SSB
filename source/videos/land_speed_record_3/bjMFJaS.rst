.. _bjMFJaS:

#######
bjMFJaS
#######

.. image:: ../../img/land_speed_3/bjMFJaS.png
   :width: 45%
   
`Original Image <https://imgur.com/bjMFJaS>`_

Explination
***********

A heavliy corrupted version of the thumbnail from :doc:`videos/eve_infinity`.  It was used a major hint for decoding :doc:`/stratish`. We can also see a smaller version of the "hello" text
next to another version of spelling "hello", a hint that stratish could be written with many different ways

Trivia
======

Throughout the 3 videos in this series of speed records the icon from the first one is used throughout the other 2 with changes in design around a shape in the center. The symbol also looks very alike to the letter I in Stratish