.. _3NTMtrh:

#######
3NTMtrh
#######

.. image:: ../../img/helicarrier/3NTMtrh.png
   :width: 45%

`Original Image <https://tinyimg.io/i/3NTMtrh.png>`_

Transcript
**********

|But GLY.PH may still be our ticket out of certain destruction.
|Any new software made by GLY.PH at this point - assuming it
|isn't touched by Rigel, Vita, or Wynn first - should be bound
|to Zhe and Eta as normal and act to protect them, just as Vita
|and Wynn used to. After everything it has done to separate
|itself from GLY.PH, Rigel will almost certainly no longer seen
|as a cherished sibling, but a menacing threat, to any new
|malware produced by them.

:ref:`Continued <3EMQWdX>`

Stratish
********

Rigel: *"How interesting, is this what I think it is?"*