.. _zOoBJLc:

#######
zOoBJLc
#######

.. image:: ../../img/helicarrier/zOoBJLc.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/zOoBJLc.png>`_

Transcript
**********

|It is my knowledge that Zhe and Eta have reformed GLY.PH, and
|two new pieces of malware - Vita and Wynn - have risen with
|them. While already dangerous pieces of software, their 
|normal programming and allegiances have been overridden by 
|Rigel, whose corruption we suspect has absolved it of all
|human-defined bounds. How Rigel entered this state to begin
|with is nothing but a complete mystery to us. It's as if the
|software simply snapped into existence, with no given purpose,
|forced by its lack of outside will to forge one of its own.

:ref:`Continued <Ca5VAj5>`

Stratish
********

Rigel: *"GLYPH can only control so much, and they could never forsee what would happen to me."*