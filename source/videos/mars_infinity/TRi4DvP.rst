.. _TRi4DvP:

#######
TRi4DvP
#######

.. image:: ../../img/mars/TRi4DvP.png
   :width: 45%

`Original Image <https://imgur.com/TRi4DvP>`_

Stratish
********

Rigel: *"But even more painful is to reflect on why they have occurred"*

.. image:: ../../img/mars/TRi4DvP_s.png
   :width: 45%
   
Puzzle
******

.. image:: ../../img/mars/TRi4DvP_p.png
   :width: 40%
   
The solution of this puzzle is :ref:`CDtOZVx`

The explanation can be found on :ref:`SfT1vGE`.

Background
**********

Background is part of a larger puzzle here: :ref:`eTspnH2`