.. _l6Cp0OX:

#######
l6Cp0OX
#######

.. image:: ../../img/helicarrier/l6Cp0OX.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/l6Cp0OX.png>`_

Transcript
**********

|We've thought about reaching out to GLY.PH itself, under the 
|assumption that they have access to a something, perhaps a 
|master switch or a control panel, that would be able to easily 
|shut down all of their malwares' operations. Yet as we've 
|recently seen, GLY.PH appear largely ambivalent to their own 
|creations, and even show little power over controlling them.

:ref:`Continued <3NTMtrh>`

Stratish
********

Rigel: *"They all think we have run our course, that the wild has crushed us"*