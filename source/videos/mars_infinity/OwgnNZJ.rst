.. _OwgnNZJ:

#######
OwgnNZJ
#######

.. image:: ../../img/mars/OwgnNZJ.png
   :width: 50%
   
`Original Image <https://../imgur.com/OwgnNZJ>`_

Stratish
********

*Hello Administrators*
   
Explination
===========

Rigel addressing us 

.. image:: ../../img/mars/OwgnNZJ_s.png
   :width: 30%
   
Alpha
*****

:ref:`CSs0zy6` in the clear.

.. image:: ../../img/mars/OwgnNZJ_1.png
   :width: 30%
   
Beta
****

:ref:`2022i8M` in the clear.

.. image:: ../../img/mars/OwgnNZJ_2.png
   :width: 30%
   
Center
******

Alpha and Beta each gave us similar images: :ref:`mnW3BZT` and :ref:`X0CIyXa`. When you overlay the central squares you get this:

.. image:: ../../img/mars/all3.png
   :width: 30%

From this you can read :ref:`aZ5EQlE`

Background
==========

The edges of this image are the edges of the Windows XP background "Home". This is part of a larger puzzle here: :ref:`eTspnH2`

.. image:: ../../img/mars/Home.jpg
   :width: 30%
   