.. _dbiiita:

#######
dbiiita
#######

.. image:: ../../img/helicarrier/dbiiita.gif
   :width: 45%
   
`Original Image <https://tinyimg.io/i/dbiiita.gif>`_

Solution
********

The turns each represent a binary number, right meaning 1 and left meaning 0.

Gives us: :ref:`y0nykfJ`

.. admonition:: TODO
   :class: danger
   
   Explain more in depth and classify output

.. image:: ../../img/helicarrier/dbiiita_1.png
   :width: 45%