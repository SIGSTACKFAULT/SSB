.. _a3AOtlN:

#######
a3AOtlN
#######

.. image:: ../../img/duna_electric/a3AOtlN.png
   :width: 45%
   
Bottom Right
************

.. image:: ../../img/duna_electric/a3AOtlN_corner.png
   :width: 45%
   
The bottom-right corner

Count the dots from the bottom

52 43 44 61 38 74 31

↓ Hex to ascii

:ref:`RCDa8t1`

Transcript
**********
::
 I am writing this to follow up on your last communication. If you weren't convinced
 that your fears were true, well, allow me to vindicate you.

 I'll put it priorly: [csrss] has contacted me.

 It was somthing much more ..... than a semi-traditional transmission. It
 ........... as our administrator, flung my way thousands upon thousands of
 ....... to change the system time.  It wanted our clock changed to every hour
 at ..................... year. The progress must have gone all the way
 through .......... I was forced to cut it off.

 I don't have to say how much this concerns me. It must know that I can't even
 begin the time changing process without administrative permission. But that's
 hardly the most troubling thing about this -- I get the overwhelming sense there
 is something we don't know about on this system, and our ..... friend is
 desperately trying to ..... it. You know what kind of programs do their work
 only on specific dates.

 How long has it been since a malware scan was run on this machine?

 -[rundll32]

Explination
===========

Csrss wanted the time changed so that :ref:`Rigel` could activate faster