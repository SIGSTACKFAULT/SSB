.. _SfT1vGE:

#######
SfT1vGE
#######

.. image:: ../../img/jool_5_infinity/SfT1vGE.png
   :width: 45%
   
`Original Image <https://imgur.com/SfT1vGE>`_

Solution
********

Courtesy of Pentasim,

This is the same puzzle as :ref:`TRi4DvP`.

You convert each character into their hexadecimal equivalents, but only take the last 2 decimal characters. 
Convert the characters into decimal equivalents, and then smack them together to form a 6-digit number. One must then find the base in which it shows the number 197420. Take those bases, and convert them from decimal to text to get: :ref:`CDtOZVx`

Explination
===========

This is essentially Breadcrumbs telling us to get off our asses and to get to work on this puzzle

.. admonition:: TODO
   :class: danger
   
   Explain puzzle on :ref`TRi4DvP`
   and also explain this in more depth with visual aid