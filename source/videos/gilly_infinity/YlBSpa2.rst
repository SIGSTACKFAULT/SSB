.. _YlBSpa2:

#######
YlBSpa2
#######

.. image:: ../../img/gilly/YlBSpa2.png
   :width: 45%

`Original Image <https://imgur.com/YlBSpa2>`_

Solution
********

First 13 icons are versions of the windows xp backgrounds. The fourteenth is the edge background of this image, "Moon Flower"

| Follow
| Ascent
| Bliss
| Follow
| Home
| Crystal
| Follow
| Autumn
| Azul
| Autumn
| Home
| Ascent
| Follow
| Moon FLower
| If you get a list of the windows xp backgrounds, put them in alphabetical order, and then number them 0 - f:

| 50 35 74 51 31 70 58
| ↓ Hex to ASCII

:ref:`P5tQ1pX`

Background
**********

Background is the Windows XP background "Moon Flower"

.. image:: ../../img/gilly/Moon_Flower.jpg
   :width: 45%
