.. _cvNi5ZL:

#######
cvNi5ZL
#######

.. image:: ../../img/kerbol_0/cvNi5ZL.png
   :width: 45%
   
`Original Image <http://tinyimg.io/i/cvNi5ZL.png>`_

Transcript
**********

::

 [smss]... [csrss]... the cowards.
 They gave up on you.
 They were too scared to use you.
 I know better. The drama is simply unnecessary...
 What's the real reason we're so hesitant?

ATV3K2C
*******

*Courtesy of ech*

| The symbols at the bottom indicate the seven symbols of the code. 
| The first two are straight forward. The third is the maximum of the array above, 
| the fourth the numbers in the triangle, the fifth the median number, 
| the second last the smallest number and the last the mean of them all. 
| The characters of the code get therefore: :ref:`ATV3K2C`

o6rhYvj
*******

By subtracting The pixel mess from this image and :ref:`ATV3K2C`, we get :ref:`o6rhYvj`.

.. image:: ../../img/kerbol_0/o6rhYvj_lead.png
   :width: 45%