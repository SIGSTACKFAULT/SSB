Stratish
========

Stratish is the language cipher used in the Gly.ph ARG, first appearing in the thumbnail of Eve Infinity on July 5th 2016. The langauge was created by Gly.ph sometime after 2003
and was used by their virus named Rigel. The purpose for this cipher seems to be only for cryptography and flair. Stratish seems to follow certain rules for how it is to be read, 
but these rules are unknown at this time. Stratish has 2 english ciphers that go together, Decals and Blocks Decals are added to blocks to complelete long words.

Writing
-------
Stratish words are split into 2 block glyphs, on the left a constant and a vowel on the right. Decals are read somehow™. Sentances are read from the first consonant and then left to 
right, if the sentance is fliped in some way you read it left to right from the unflipped rotation.

.. image:: img/stratish_page/stratish_legend.png
   :width: 40%
Reading
--------
Disclaimer: We have no idea how to read

We have a ideas as to how stratish is to be read, but hardly anything solid. However we do know a few inklings as to how it is meant to be read, most notably the Atlas and a few confirmed rules. 
The Atlas is a strangely shaped image that presumably outlines how Stratish is meant to be read, this was told to us by Wynn. The 3 layered squares
likely pertain towards the smaller stratish that can be found within words. The lines with boxes likely refer to decorations, and the decorations seem to follow the Atlas to a certain extent. 
Certain priorties have been found, such as Fr, Th and a few others. When this occurs it overrides any other priority. 