.. _foJcS7x:

#######
foJcS7x
#######

.. image:: ../../img/helicarrier/foJcS7x.png
   :width: 45%

`Original Image <https://tinyimg.io/i/foJcS7x.png>`_

Solution
********

Colored squares on the end of branches represent ones and zeroes. If the square is on the right, its 1, on the left, its 0. The order is determined by the hue of the square.

:ref:`E0uk3Az`