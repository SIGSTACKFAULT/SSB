.. _vBJKNuE:

#######
vBJKNuE
#######

.. image:: ../../img/mars/vBJKNuE.png
   :width: 45%
   
`Original Image <https://imgur.com/vBJKNuE>`_

Shows similarities to the frame from :ref:`Building a Secret Base`. Bars are in different horizontal places but they are the same length and number.

Therefore the solution should still be :ref:`w35YYc0`. So this was likely just Bread trying to tell us to finish the puzzle, or to simply show the conversation shown below.

Above bars
**********

|V0hBVCBFWEFDVExZIElTIFRISVMsIFNNU1M/
|↓ base64 to ASCII

What exactly is this, smss?

Below bars
**********

|VkVSWSBJTVBPUlRBTlQsIElTIFdIQVQgSVQgSVM=
|↓ base64 to ASCII

Very important, is what it is

Explination
===========

Conversation between :ref:`smss` and another program. Likely Task Manager, :ref:`lsass` or the new :ref:`csrss`.