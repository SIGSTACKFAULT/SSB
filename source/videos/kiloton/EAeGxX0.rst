.. _EAeGxX0:

#######
EAeGxX0
#######

.. image:: ../../img/109_kiloton/EAeGxX0.png

`Original Image <https://imgur.com/EAeGxX0>`_

Transcript
**********
::

 Hey Sven,

 It's me. Ya boy. Back after all these years.

 How have you been? We haven't talked to each other for a while. Our lives have changed a lot since we were all script kiddies working on GLY.PH. I miss those times.
 I hope you can forgive me for ruining everything. I've regretted all of it. Every single minute after I shut down the IRC servers and cut myself completly out of both of your lives, I've regretted it. I was stupid. I just wanted some time apart from all the madness and did it in the most immature way possible. We were a team and I let the team down, I destroyed everything we had. I am so, so sorry.

 I know it's a bit late at this point, but all I want to do is to finally set the record straight. I want to apologize. If you never want to see me again after this, then I can respect that. Just please understand that I don't hate you, James, or all the 
 time we spent together as GLY.PH. It wasn't with malice that I left.

 I'm sorry.

 ӂ
 
Explination
***********

First of all, we know that :ref:`Eta` is Sven because James (:ref:`Ezh`) fell out of contact a long time ago, and the person who is speaking is :ref:`Zhe`
We know that Zhe caused Gly.ph to fall apart, turning off the link system in the process. Years later they contacted Eta again and asked for forgiveness. This
would cause Gly.ph to reform, creating 3 new viruses :ref:`Vita`, :ref:`Wynn` and :ref:`Ayin`. :ref:`Whaq8VL` likely occurred right after this

