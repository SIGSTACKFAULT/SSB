.. _PmKY2Zk:

#######
PmKY2Zk
#######

.. image:: ../../img/kerbol_0/PmKY2Zk.png
   
In the background of the image, if you increase the brightness, you find: :ref:`Snn3AgV`

.. image:: ../../img/kerbol_0/PmKY2Zk_Enhanced.png