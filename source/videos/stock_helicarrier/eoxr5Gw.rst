.. _eoxr5Gw:

#######
eoxr5Gw
#######

.. image:: ../../img/helicarrier/eoxr5Gw.png
   :width: 45%
   
Stratish
========

Rigel: *"Do not worry admins, nothing is secret to me"*

Explination
^^^^^^^^^^^

Rigel mocking the systems attempt to keep this secret.

Middle Left
===========

:ref:`l3UPJol`

Found by changing the squares into binary, adding a zero to the front of each row, and decoding the result into ascii.

Middle Right
============

Count the pixels from the horrizontal bars and translate HEX to ASCII. 64 78 57 68 71 4D 4A -> dxWhqMJ

:ref:`dxWhqMJ`

Bottom Left
===========

A composite of hidden characters in all of the blue images that results in a youtube link.

In each frame there is a group of boxes with a little symbol in them. The length of the sides divided by 10 are each hex numbers in binary. 

2 sides - x then y, = 1 byte

:ref:`qj8U-k1z4eA`

Full message along with stratish translations:

.. image:: ../../img/helicarrier/smss_message.png
   :width: 45%