.. _tryQCXY:

#######
tryQCXY
#######

.. image:: ../../img/helicarrier/tryQCXY.png
   :width: 45%
   
`Original Image <https://tinyimg.io/i/tryQCXY.png>`_

The boxes outline the general shape of a letter. Trial-and-error-until-it-works leads to: :ref:`zOoBJLc`