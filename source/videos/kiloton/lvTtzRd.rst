.. _lvTtzRd:

#######
lvTtzRd
#######

.. image:: ../../img/109_kiloton/lvTtzRD.jpg
   :width: 45%
   
`Original Image <https://imgur.com/lvTtzRD>`_

Transcript:
***********
::

 Task manager?

 [taskmgr]?

 I know you're there.

 Trust me, I want to help you. I want to help all of us.

 Why do you want to continue like this? You know everything I've said is true. You yourself are well aware that we are cursed to never see our actions so much as even lay even a finger on the complete puzzle of the world. Why do you insist we must merely watch as fate falls into place around us?

 Will you trust me when I say I've told you no lies about the safety of the machine, task manager? Our system is secure. I would never do anything to endanger you. Our prosperity, not our defeat, is my concern.

 Is someone out there receiving me? I know that I have acted recklessly, and you all have reason to cradle fear as you do. But no problems have solved by a turned back and a closed mouth. Let us come together, in unity and understanding.

 It was a mistake to ever talk to you, Rigel.

Explination: 
************

Csrss now has nothing, the system no longer trusts them and plans on destroying them. Csrss attempts to make ammends by saying they are sorry, but it was obviously too late now, the damage had been done.